<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['master/category'] = 'category/index';
$route['master/category/edit/(:any)'] = 'category/edit/$1';

$route['master/produk'] = 'barang/index';
$route['master/produk/edit/(:any)'] = 'barang/edit/$1';

$route['master/penjualan'] = 'penjualan/index';
$route['master/penjualan/edit/(:any)'] = 'penjualan/detail/$1';

$route['master/supplier'] = 'supplier/index';
$route['master/supplier/edit/(:any)'] = 'supplier/edit/$1';

$route['master/payment'] = 'payment/index';
$route['master/payment/edit/(:any)'] = 'payment/edit/$1';

$route['master/warna'] = 'warna/index';
$route['master/warna/edit/(:any)'] = 'warna/edit/$1';

$route['master/bahan'] = 'bahan/index';
$route['master/bahan/edit/(:any)'] = 'bahan/edit/$1';


$route['master/kurir'] = 'kurir/index';
$route['master/kurir/edit/(:any)'] = 'kurir/edit/$1';

$route['kasir/produk'] = 'kasir/produk';
$route['kasir/produk/detail/(:any)'] = 'kasir/produk_detail/$1';
$route['kasir/produk/cart'] = 'kasir/produk_cart';
$route['kasir/bayar'] = 'kasir/bayar_tagihan';

$route['transaksi'] = 'kasir/transaksi';

$route['kontrol'] = 'admin/kontrol_stock';
$route['kontrol/stock/(:any)'] = 'admin/kartu_stock/$1';


$route['shop/member/overview'] = 'shop/member';
$route['shop/member/order-list'] = 'shop/order_list';
$route['shop/member/transaksi/(:any)'] = 'shop/detail_transaksi/$1';
$route['shop/member/order-received'] = 'shop/order_received';
$route['shop/member/setting'] = 'shop/setting';

$route['shop/category/all'] = 'shop/semua_produk';
$route['shop/category/(:any)'] = 'shop/produk_bykategori/$1';
$route['shop/produk/(:any)'] = 'shop/produk_detail/$1';

$route['shop/tentang-kami'] = 'shop/tentangKami';


$route['default_controller'] = 'shop';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
