<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('Category_model');
        $this->load->model('Barang_model');
        $this->load->model('Supplier_model');
        $this->load->model('Penjualan_model');
        $this->load->model('Penjualan_detail_model');
        $this->load->model('Stock_model');


        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $transaksi_terakhir = $this->Penjualan_model->penjualan_terakhir();
        $data_transaksi = [];
        if ($transaksi_terakhir == null) {
            $data_transaksi = [];
        } else {
            foreach ($transaksi_terakhir as $key) {

                switch ($key->status) {

                    case 0:
                        $key->order_status = "Belum Dibayar";
                        break;
                    case 1:
                        $key->order_status = "Sudah Dibayar";
                        break;
                    case 2:
                        $key->order_status = "Sedang Dikirim";
                        break;
                    case 3:
                        $key->order_status = "Sukses Dikirim";
                        break;
                    case 4:
                        $key->order_status = "Di Tolak";
                        break;
                    default:
                        $key->order_status = "Belum Dibayar";
                        break;
                }

                if ($key->member == "ya") {
                    $key->name_user_order = $this->Penjualan_model->member_byid($key->user)->get()->row()->nama;
                } else {
                    $key->name_user_order = $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->nama;
                }

                if ($key->approvement == 0) {
                    $key->admin_name = "Admin";
                } else {
                    $key->admin_name = $this->Penjualan_model->approvement_byid($key->approvement)->get()->row()->name;
                }

                $data_transaksi[] = $key;
            }
        }



        //var_dump($data_transaksi);

        $data['transaksi_terakhir'] = $data_transaksi;

        $data['main_content'] = 'admin/main';
        $data['page_title'] = 'Halaman Admin';
        $this->load->view('template', $data);
    }

    public function cek(){

        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth(6);
        var_dump($totalPendapatan);
    }

    public function print_produk_terlaris($month){
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->set_option('isRemoteEnabled', TRUE);
        $this->pdf->filename = "print_produk_terlaris.pdf";
        $this->pdf->load_view('laporan/laporan_produk_terlaris', ['data' => $topProduk]);
    }

    public function dashboard($month)
    {

        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->jumlah_produk_order_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }
}
