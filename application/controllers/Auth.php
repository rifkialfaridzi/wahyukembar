<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Auth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data_session = $this->session->userdata;

		if (!empty($data_session['level'])) {

			switch ($data_session['level']) { // Login Sebagai APA ? 
				case "1":
					redirect('admin');
					break;
				case "2":
					redirect('admin');
					break;
				case "3":
					redirect('shop/member');
					break;
			}
		}
		$this->load->view('auth/login');
	}

	function login()
	{ // Cek Login

		$username = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES); // cek special caracter
		$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

		$cek = $this->auth_model->auth_login($username, $password); // ambil data user login

		if ($cek->num_rows() > 0) { //jika login sebagai dosen
			$data = $cek->row_array();

			$this->session->set_userdata('level', $data['level']);
			$this->session->set_userdata('id', $data['id']);
			$this->session->set_userdata('username', $data['username']);
			$this->session->set_userdata('logged_in', true);

			if ($data['level'] == '1') { // Jika Akses admin gudang
				redirect('admin');
			} elseif ($data['level'] == '2') { // Jika Akses Kasir
				redirect('admin');
			} elseif ($data['level'] == '3') { // Jika Akses Owner
				redirect('shop');
			}
		} else { //jika login gagal
			$url = base_url();
			$this->session->set_flashdata('pesan', 'Username Atau Password Salah');
			redirect($url);
		}
	}

	function do_registration($type)
	{
		// $digits = 6;
		// echo str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);

		$this->form_validation->set_rules('username', 'username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[user.email]');
		$this->form_validation->set_rules('tlp', 'tlp', 'required|is_unique[user.tlp]');
		
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('password_conf', 'password', 'required|matches[password]');

		if ($type == 1) { // CREATE ADMIN LEVEL 2

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect(site_url('user'));
			} else {

				$data_user = array(
					'name' => $this->input->post('name', TRUE),
					'username' => $this->input->post('username', TRUE),
					'password' => md5($this->input->post('password', TRUE)),
					'email' => $this->input->post('email', TRUE),
					'tlp' => $this->input->post('tlp', TRUE),
					'level' => 2,
					'image' => "",
					'created_at' => date('Y-m-d'),
				);


				$this->User_model->insert($data_user);
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect(site_url('user'));
			}
		} else {  // CREATE MEMBER

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect(site_url());
			} else {


				$data_user = array(
					'name' => $this->input->post('name', TRUE),
					'username' => $this->input->post('username', TRUE),
					'password' => md5($this->input->post('password', TRUE)),
					'email' => $this->input->post('email', TRUE),
					'tlp' => $this->input->post('tlp', TRUE),
					'level' => 3,
					'image' => "",
					'created_at' => date('Y-m-d'),
				);

				$last_id = $this->User_model->insert($data_user);

				$data_additional = array(
					'user' => $last_id,
					'alamat_penerima' => "-",
					'nama_penerima' => $this->input->post('name', TRUE)
				);
				$this->User_model->insert_user_detail($data_additional);

				$this->session->set_flashdata('pesan', 'Yey ! Pendaftaran Berhasil, Silahkan Masuk');
				redirect(site_url('shop/masuk'));
			}
		}
	}

	public function forget()
	{
		// $digits = 6;
		// echo str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
		$tlp = $this->input->post('tlp', TRUE);
		$data_user = $this->User_model->get_by_tlp($tlp);


		$token_verifikasi = substr(sha1(rand()), 0, 30);

		if ($data_user == null) {
			$this->session->set_flashdata('pesan', 'Nomor Whatsapp Tidak Di Temukan');
			redirect(site_url('shop/lupa_password'));
		} else {
			$this->User_model->update($data_user->id, ['recovery' => 1, 'token_verifikasi' => $token_verifikasi]);
			$this->session->set_flashdata('pesan', 'Data Ditemukan, Pengajuan Atur Ulang Password Sedang Diproses');
			redirect(site_url('shop/lupa_password'));
		}
	}

	public function do_recovery($token)
	{
		$data_user = $this->User_model->get_by_token_verifikasi($token);

		if ($data_user == null) {
			$this->session->set_flashdata('pesan', 'Kode Token Tidak Valid');
			redirect(site_url());
		} else {

			$this->form_validation->set_rules('password', 'password', 'trim');
			$this->form_validation->set_rules('password_conf', 'password', 'matches[password]');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect(site_url('shop/recovery/').$token);
			} else {
				$this->User_model->update($data_user->id, ['password' => md5($this->input->post('password', TRUE)),'recovery' => 0, 'token_verifikasi' => ""]);
			}

			
			$this->session->set_flashdata('pesan', 'Kata Sandi Sukses Di Atur Ulang');
			redirect(site_url('shop/masuk'));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect('/', 'refresh');
	}



	public function json()
	{
		header('Content-Type: application/json');
		echo $this->Barang_model->json();
	}
}
