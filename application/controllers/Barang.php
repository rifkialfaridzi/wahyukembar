<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barang extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Barang_model');
		$this->load->model('Category_model');
		$this->load->model('Supplier_model');
		$this->load->model('Warna_model');
		$this->load->model('Bahan_model');

		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function generate_code()
	{
		$query = "SELECT max(kode) as maxKode FROM barang";
		$max_code = $this->db->query($query)->row()->maxKode;
		$noUrut = (int) substr($max_code, 4, 4);
		$noUrut++;
		$char = "SKU";
		$kodeBarang = $char . sprintf("%04s", $noUrut);
		return $kodeBarang;
	}


	public function index()
	{
		$data['main_content'] = 'produk/main';
		$data['page_title'] = 'Halaman Produk';


		$data['code_barang'] = $this->generate_code();
		$data['kategori'] = $this->Category_model->get_all();
		$data['supplier'] = $this->Supplier_model->get_all();
		$data['warna'] = $this->Warna_model->get_all();
		$data['bahan'] = $this->Bahan_model->get_all();
		//var_dump($data);
		$this->load->view('template', $data);
	}

	
    public function print_produk(){

        $produk =  $this->Barang_model->product_allcategory();
		$data_session = $this->session->userdata;
		$date = date("Y-m-d");
        if ($produk == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        } else {
           
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_produk-".$date."pdf";
            $this->pdf->load_view('laporan/laporan_produk',['data'=>$produk,'username' =>$data_session['username']]);
        }
 
     }


	public function json()
	{
		header('Content-Type: application/json');

		$barang =  $this->Barang_model->json();
		//var_dump($barang);
		$data['draw'] = 0;
        $data['recordsTotal'] = $barang == null ? [] : count($barang);
        $data['recordsFiltered'] = $barang == null ? [] : count($barang);
        $data['data'] = $barang == null ? [] : $barang;
		
        echo json_encode($data);
	}
	public function product_bystock()
	{
		header('Content-Type: application/json');
		echo $this->Barang_model->product_bystock();
	}

	public function generate_qr($kode){

		$kode_barang = $kode;

		$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/uploads/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name=$kode_barang.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = $kode_barang; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
 

	}

	public function print_qr($id){

		
		$produk = $this->Barang_model->get_by_id($id);

		$data = [
			'image'=>'assets/uploads/qrcode/'.$produk->kode.".png",
			'price'=>'Rp.'.number_format($produk->harga_penjualan,2,',','.'),
			'produk_name'=>$produk->nama,
			'kode'=>$produk->kode,
			'company'=>'Batik Wahyu Kembar'
		];
	
		$this->load->library('pdf');
	
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
		$this->pdf->filename = "print_qrcode_".$produk->nama.".pdf";
		$this->pdf->load_view('produk/print_qr', $data);
	
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Nama Produk Tidak Boleh Sama');
			redirect(site_url('master/produk'));
			//echo validation_errors();
		} else {
			$config['upload_path'] = './assets/uploads/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']  = '2048';
			$config['remove_space'] = TRUE;

			$this->load->library('upload', $config); // Load konfigurasi uploadnya
			if ($this->upload->do_upload('image')) { // Lakukan upload dan Cek jika proses upload berhasil
				// Jika berhasil :
				$data = $this->upload->data();
				$message = $this->upload->data();
			} else {
				// Jika gagal :
				$data = ['file_name'=>''];
				$message = $this->upload->display_errors();
			}
		
			$data = array(
				'kode' => $this->input->post('kode', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'supplier' => $this->input->post('supplier', TRUE),
				'category' => $this->input->post('category', TRUE),
				'warna' => $this->input->post('warna', TRUE),
				'bahan' => $this->input->post('bahan', TRUE),
				'ukuran' => $this->input->post('ukuran', TRUE),
				'image' => $data['file_name'],
				'stock' => $this->input->post('stock', TRUE),
				'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
				'min_stock' => $this->input->post('min_stock', TRUE),
				'note' => $this->input->post('note', TRUE),
				'berat' => $this->input->post('berat', TRUE),
				'created_at' => date("Y-m-d"),
			);

			
			$this->Barang_model->insert($data);
			$this->generate_qr($this->input->post('kode', TRUE));
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('master/produk'));
		}
	}

	public function edit($id)
	{
		$row = $this->Barang_model->get_by_id($id);


		if ($row) {
			$data = array(
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'nama' => set_value('nama', $row->nama),
				'category' => set_value('category', $row->category),
				'warna_id' => set_value('warna', $row->warna),
				'bahan_id' => set_value('bahan', $row->bahan),
				'supplier_id' => set_value('supplier', $row->supplier),
				'image' => set_value('image', $row->image),
				'stock' => set_value('stock', $row->stock),
				'berat' => set_value('berat', $row->berat),
				'ukuran' => set_value('ukuran', $row->ukuran),
				'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
				'min_stock' => set_value('min_stock', $row->min_stock),
				'note' => set_value('note', $row->note),
				'created_at' => set_value('created_at', $row->created_at),
				'main_content' => 'produk/update',
				'page_title' => 'Edit Barang'
			);
		
		$data['kategori'] = $this->Category_model->get_all();
		$data['supplier'] = $this->Supplier_model->get_all();

		$data['warna'] = $this->Warna_model->get_all();
		$data['bahan'] = $this->Bahan_model->get_all();


			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('master/produk'));
		}
	}

	public function update_action($id)
	{
		$barang = $this->Barang_model->get_by_id($id);
		$is_unique_name = $this->input->post('nama', TRUE) != $barang->nama ? '|is_unique[barang.nama]' : '';
		
		$this->load->helper('form');
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'required'.$is_unique_name);//|edit_unique[barang.nama.' . $id . ']
		$this->form_validation->set_rules('supplier', 'Supplier', 'trim|required');
		$this->form_validation->set_rules('category', 'category', 'trim|required');
		$this->form_validation->set_rules('warna', 'warna', 'trim|required');
		$this->form_validation->set_rules('bahan', 'bahan', 'trim|required');
		$this->form_validation->set_rules('ukuran', 'ukuran', 'trim|required');
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('berat', 'berat', 'trim|required');
		$this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
		$this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
		$this->form_validation->set_rules('note', 'note', 'trim|required');
		$this->form_validation->set_rules('created_at', 'created at', 'trim');

		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {

			if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
				
			$config['upload_path'] = './assets/uploads/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']  = '2048';
			$config['remove_space'] = TRUE;

			$this->load->library('upload', $config); // Load konfigurasi uploadnya
			if ($this->upload->do_upload('image')) { // Lakukan upload dan Cek jika proses upload berhasil
				// Jika berhasil :
				$data = $this->upload->data();
				$message = $this->upload->data();
			} else {
				// Jika gagal :
				$data = ['file_name'=>''];
				$message = $this->upload->display_errors();
			}
		
			$data = array(
				'kode' => $this->input->post('kode', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'supplier' => $this->input->post('supplier', TRUE),
				'category' => $this->input->post('category', TRUE),
				'warna' => $this->input->post('warna', TRUE),
				'bahan' => $this->input->post('bahan', TRUE),
				'ukuran' => $this->input->post('ukuran', TRUE),
				'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
				'image' => $data['file_name'],
				'stock' => $this->input->post('stock', TRUE),
				'berat' => $this->input->post('berat', TRUE),
				'min_stock' => $this->input->post('min_stock', TRUE),
				'note' => $this->input->post('note', TRUE),
			);
			}else{
				$data = array(
					'kode' => $this->input->post('kode', TRUE),
					'nama' => $this->input->post('nama', TRUE),
					'supplier' => $this->input->post('supplier', TRUE),
					'category' => $this->input->post('category', TRUE),
					'warna' => $this->input->post('warna', TRUE),
					'bahan' => $this->input->post('bahan', TRUE),
					'ukuran' => $this->input->post('ukuran', TRUE),
					'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
					'stock' => $this->input->post('stock', TRUE),
					'berat' => $this->input->post('berat', TRUE),
					'min_stock' => $this->input->post('min_stock', TRUE),
					'note' => $this->input->post('note', TRUE));
			}
			

			$this->Barang_model->update($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('master/produk'));
		}
	}

	public function delete($id)
	{
		$row = $this->Barang_model->get_by_id($id);

		if ($row) {
			$this->Barang_model->delete($id);
			$this->session->set_flashdata('pesan', 'Delete Record Success');
			redirect(site_url('master/produk'));
		} else {
			$this->session->set_flashdata('pesan', 'Record Not Found');
			redirect(site_url('master/produk'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required|is_unique[barang.nama]');
		$this->form_validation->set_rules('supplier', 'unit', 'trim|required');
		$this->form_validation->set_rules('category', 'category', 'trim|required');
		$this->form_validation->set_rules('warna', 'warna', 'trim|required');
		$this->form_validation->set_rules('bahan', 'bahan', 'trim|required');
		$this->form_validation->set_rules('ukuran', 'ukuran', 'trim|required');
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
		$this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
		$this->form_validation->set_rules('berat', 'berat', 'trim|required');
		$this->form_validation->set_rules('note', 'note', 'trim|required');
		$this->form_validation->set_rules('created_at', 'created at', 'trim');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

}

/* End of file Barang.php */
/* Location: ./application/controllers/Barang.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:19 */
/* http://harviacode.com */
