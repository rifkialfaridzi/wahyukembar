<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        // if ((!$this->session->userdata('logged_in'))  || $data_session['level'] != 2) {
        //     redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        // }

        $this->load->model('Category_model');
        $this->load->model('Barang_model');

        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function scan_product()
    { //fungsi Add To Cart

        $params = json_decode(file_get_contents('php://input'), true);

        $data_produk = $this->Barang_model->get_data_product_bykode($params['kode_product'])->get()->row();
		
		if($data_produk == null){
			$this->session->set_flashdata('pesan', 'Produk <strong>' . $data_produk->nama . '</strong> Tidak Ditemukan');
		}else{
		$data = array(
            'id' => $data_produk->id,
            'name' => $data_produk->nama,
            'price' => $data_produk->harga_penjualan,
            'qty' => 1,
        );
        $this->cart->insert($data);
		
        $this->session->set_flashdata('pesan', 'Produk <strong>' . $data_produk->nama . '</strong> Suskses Masuk Ke Keranjang');
		}
        
        // redirect(site_url('pembelian/masuk'));

    }

    function add_to_cart()
    { //fungsi Add To Cart

        $params = json_decode(file_get_contents('php://input'), true);

        $data = array(
            'id' => $params['id'],
            'name' => $params['nama'],
            'price' => $params['harga'],
            'qty' => $params['qty'],
        );

        $this->cart->insert($data);
    }

    function update_cart()
    { //fungsi Add To Cart

        $params = json_decode(file_get_contents('php://input'), true);
        $stockProduct = $this->Barang_model->get_by_id($params['productid']);

        if ($params['qty'] > ($stockProduct->stock-$stockProduct->stock)) {
            $this->session->set_flashdata('pesan', 'Produk <strong>' . $stockProduct->nama . '</strong> Stock Tidak Mencukupi');
        } else {

            $data = array(
                'rowid' => $params['rowid'],
                'qty' => $params['qty'],
                'productid' => $params['productid'],
            );

            $this->cart->update($data);
            if ( $params['qty'] == 0) {
                $this->session->set_flashdata('pesan', 'Produk <strong>' . $stockProduct->nama . '</strong> Sukses Di Hapus Dari Keranjang');
            }else{
                $this->session->set_flashdata('pesan', 'Produk <strong>' . $stockProduct->nama . '</strong> Sukses Di Tambahkan Ke Keranjang');
            }

           
            
        }
    }

    function count_cart()
    { //load data cart
        echo count($this->cart->contents());
    }

    function load_cart()
    { //load data cart
        var_dump($this->cart->contents());
    }
}
