<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        // if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
        //     redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        // }

        $this->load->model('User_model');
        $this->load->model('Category_model');
        $this->load->model('Barang_model');
        $this->load->model('Supplier_model');
        $this->load->model('Penjualan_model');
        $this->load->model('Penjualan_detail_model');
        $this->load->model('Chat_model');


        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {

        $data['main_content'] = 'member/main';
        $data['page_title'] = 'Halaman Member';

        $this->load->view('template', $data);
    }
    public function kirim_chat($id){

        $this->load->helper('form');
        $this->form_validation->set_rules('content', 'content', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('pesan', 'Pesan Masih Kosong');
           redirect(site_url('member/detail/'.$id));
          
       } else {
           $data = array(
            'send_to' => $id,
            'send_by' => 1,
            'content' => $this->input->post('content', TRUE),
            'tanggal' => date("Y-m-d H:i:s"),
           );
           
           $this->Chat_model->insert($data);
           $this->User_model->update($id, ["recovery" =>0]);
           $this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
          redirect(site_url('member/detail/'.$id));
       }
    }

    public function detail($id)
    {
        $data_user = $this->User_model->get_data_user_byid($id)->get()->row();
        $dataPenjualan = $this->Penjualan_model->penjualan_bymember($id);
        $dataChat = $this->Chat_model->get_chat_from_admin($id);
        if ($data_user == null) {
            $this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
            redirect(site_url('member'));
        } else {
            $data['main_content'] = 'member/detail';
            $data['page_title'] = 'Halaman Detail Member';
            $data['data_user'] = $data_user;
            $data['data_chat'] = $dataChat;
            $data['jumlah_transaksi'] = count($dataPenjualan);

            $this->load->view('template', $data);
        }
    }

    public function delete($id)
    {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $this->User_model->delete($id);
            $this->session->set_flashdata('pesan', 'Delete Record Success');
            redirect(site_url('member'));
        } else {
            $this->session->set_flashdata('pesan', 'Record Not Found');
            redirect(site_url('member'));
        }
    }

    public function recovery_password()
    {

        $tlp = $this->input->post('tlp', TRUE);
        $data_user = $this->User_model->get_by_tlp($tlp);
       
		if ($data_user == null) {
			$this->session->set_flashdata('pesan', 'Nomor Whatsapp Tidak Di Temukan');
			redirect(site_url('shop/lupa_password'));
		}else{
			$this->User_model->update($data_user->id,['recovery'=>2]);
            $this->session->set_flashdata('pesan', 'Data Ditemukan, Pengajuan Atur Ulang Password Sedang Diproses');
			redirect(site_url('member/detail/'.$data_user->id));
        }
        
    }

    public function member_json()
    {
        header('Content-Type: application/json');
        $member = $this->User_model->user_member_json();

        $data['draw'] = 0;
        $data['recordsTotal'] = $member == null ? [] : count($member);
        $data['recordsFiltered'] = $member == null ? [] : count($member);
        $data['data'] = $member == null ? [] : $member;
		
        echo json_encode($data);
    }
    public function transaksi_json($id_member)
    {
        header('Content-Type: application/json');
      
        $dataPenjualan = $this->Penjualan_model->penjualan_bymember($id_member);

        $data['draw'] = 0;
        $data['recordsTotal'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['recordsFiltered'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['data'] = $dataPenjualan == null ? [] : $dataPenjualan;
        // var_dump($stockmup);
        echo json_encode($data);
       
    }

    
    public function print_member(){

        $member =  $this->User_model->user_member();
		$date = date("Y-m-d");
        $data_session = $this->session->userdata;
        if ($member == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        } else {
           
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_member-".$date."pdf";
            $this->pdf->load_view('laporan/laporan_member',['data'=>$member,'username' =>$data_session['username']]);
        }
 
     }

}
