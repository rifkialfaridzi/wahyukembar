<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('Payment_model');
        $this->load->model('Penjualan_detail_model');
        $this->load->model('Penjualan_model');
        $this->load->model('User_model');
        $this->load->model('Category_model');
        $this->load->model('Kurir_model');
        $this->load->model('Shop_model');
        $this->load->model('Barang_model');
        $this->load->model('Diskon_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $data['main_content'] = 'penjualan/main';
        $data['page_title'] = 'Halaman Penjualan Produk';

        $this->load->view('template', $data);
    }

    public function detail($id_transaksi)
    {

        $penjualan = $this->Penjualan_model->penjualan_by_idtransaksi($id_transaksi)->get()->row();
        $pembayaran  = $this->Payment_model->get_all();

        if ($penjualan->member == "ya") { // PENJUALN SEBAGAI MEMBER

            $data_penjualan = $penjualan;
            $data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
            $data_penjualan_member = $this->Penjualan_model->penjualan_member($id_transaksi)->get()->row();
            $data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
            $data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
            $data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

            $data['data_penjualan'] = $data_penjualan;
            $data['data_penjualan_detail'] = $data_penjualan_detail;
            $data['data_penjualan_pelanggan'] = $data_penjualan_member;
            $data['data_penjualan_diskon'] = $data_penjualan_diskon;
            $data['data_penjualan_payment'] = $data_penjualan_payment;
        } else { // PENJUALN SEBAGAI PENGUNJUNG
            $data_penjualan = $penjualan;
            $data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
            $data_penjualan_pelanggan = $this->Penjualan_model->penjualan_pelanggan($id_transaksi)->get()->row();
            $data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
            $data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
            $data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

            $data['data_penjualan'] = $data_penjualan;
            $data['data_penjualan_detail'] = $data_penjualan_detail;
            $data['data_penjualan_pelanggan'] = $data_penjualan_pelanggan;
            $data['data_penjualan_diskon'] = $data_penjualan_diskon;
            $data['data_penjualan_kurir'] = $data_penjualan_kurir;
            $data['data_penjualan_payment'] = $data_penjualan_payment;
        }

        $data['data_metode_pembayaran'] = $pembayaran;
        $data['data_detail_pembayaran'] = $this->Penjualan_model->get_detail_pembayaran($penjualan->id)->get()->row();


        //print_r($data);
        $this->load->view('penjualan/detail', $data);
    }

    public function penjualan_all_json()
    {
        $dateStart = $this->input->get('start');
        $dateEnd = $this->input->get('end');
        $status = $this->input->get('status');

        if ($dateStart == null || $dateEnd == null || $status == null) {
            $dataPenjualan = $this->Penjualan_model->semua_penjualan(null, "all")->get()->result();
        } else {

            if ($status == null) {
                $dataPenjualan = $this->Penjualan_model->semua_penjualan(['dateStart' => $dateStart, 'dateEnd' => $dateEnd], "all")->get()->result();
            } else {
                $dataPenjualan = $this->Penjualan_model->semua_penjualan(['dateStart' => $dateStart, 'dateEnd' => $dateEnd], $status)->get()->result();
            }
        }

        $data_penjualan = [];

        foreach ($dataPenjualan as $key) {

            if ($key->member == "ya") {
                $key->nama_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->nama;
                $key->alamat_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->alamat_penerima;
                $key->tlp_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->tlp;
            } else {
                $key->nama_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->nama;
                $key->alamat_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->alamat_penerima;
                $key->tlp_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->tlp;
            }

            $data_penjualan[] = $key;
        }
        $data['draw'] = 0;
        $data['recordsTotal'] = $data_penjualan == null ? [] : count($data_penjualan);
        $data['recordsFiltered'] = $data_penjualan == null ? [] : count($data_penjualan);
        $data['data'] = $data_penjualan == null ? [] : $data_penjualan;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    public function ubah_status($id_transaksi)
    {
        header('Content-Type: application/json');
        $data_session = $this->session->userdata;

        // UPDATE DATA
        $data = json_decode(file_get_contents('php://input'));
        $data_penjualan_detail = $this->Penjualan_model->penjualan_detail($id_transaksi)->get()->result();

        if ($data->status == "2" || $data->status == "3") {

            foreach ($data_penjualan_detail as $key) {
                $this->Barang_model->update_stock($key->barang, $key->jumlah);
            }
            $this->Penjualan_model->update_status($id_transaksi, ['status' => $data->status, 'approvement' => $data_session['id']]);
        } else {
            $this->Penjualan_model->update_status($id_transaksi, ['status' => $data->status]);
        }
    }

    public function penjualan_byrange_json($dateStart, $dateEnd)
    {

        $dataPenjualan = $this->Penjualan_model->penjualan_range($dateStart, $dateEnd)->get()->result();

        $data['draw'] = 0;
        $data['recordsTotal'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['recordsFiltered'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['data'] = $dataPenjualan == null ? [] : $dataPenjualan;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    public function print_penjualan() // CETAK LAPORAN 
    {

        $dateStart = $this->input->get('start');
        $dateEnd = $this->input->get('end');
        $status = $this->input->get('status');

        if ($dateStart == null || $dateEnd == null || $status == null) {
            $dataPenjualan = $this->Penjualan_model->semua_penjualan_print(null, "all")->get()->result();
        } else {

            if ($status == null) {
                $dataPenjualan = $this->Penjualan_model->semua_penjualan_print(['dateStart' => $dateStart, 'dateEnd' => $dateEnd], "all")->get()->result();
            } else {
                $dataPenjualan = $this->Penjualan_model->semua_penjualan_print(['dateStart' => $dateStart, 'dateEnd' => $dateEnd], $status)->get()->result();
            }
        }

        $data_penjualan = [];
        $omset = 0;
        foreach ($dataPenjualan as $key) {

            if ($key->member == "ya") {
                $key->nama_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->nama;
                $key->alamat_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->alamat_penerima;
                $key->tlp_penerima =  $this->Penjualan_model->member_byid($key->user)->get()->row()->tlp;
            } else {
                $key->nama_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->nama;
                $key->alamat_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->alamat_penerima;
                $key->tlp_penerima =  $this->Penjualan_model->non_member_byid($key->pelanggan)->get()->row()->tlp;
            }

            switch ($key->status) {
                case 0:
                    $key->order_status = "Belum Dibayar";
                    break;
                case 1:
                    $key->order_status = "Sudah Dibayar";
                    break;
                case 2:
                    $key->order_status = "Sedang Dikirim";
                    break;
                case 3:
                    $key->order_status = "Sudah Sampai";
                    break;
                case 4:
                    $key->order_status = "Selesai";
                    break;

                default:
                    $key->order_status = "Belum Dibayar";
                    break;
            }

            $omset += $key->total_penjualan;

            $data_penjualan[] = $key;
        }

        $date = ['dateStart' => $dateStart, 'dateEnd' => $dateEnd,];
        $data_session = $this->session->userdata;
        if ($dataPenjualan == null) {
            // $this->session->set_flashdata('message', 'Record Not Found');
            // redirect(site_url('kasir/transaksi'));
        } else {
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_penjualan-" . $dateStart . "-" . $dateEnd . ".pdf";
            $this->pdf->load_view('laporan/laporan_transaksi', ['data' => $dataPenjualan, 'date' => $date,'omset' => $omset,'username' =>$data_session['username']]);
        }
    }

    public function data_penjualan()
    {
        $a = 55;
        $data_penjualan = [];

        for ($i = 0; $i < $a; $i++) {

            $data_penjualan[] =

                [
                    "id" => $i,
                    "image" => "assets/uploads/FROYO1.jpg",
                    "nama" => "Kebaya A " . $i,
                    "harga" => "10000" . $i,
                    "note" => "Bahan: Broklat A" . $i

                ];
        }

        $data = [
            'totalItems' => 55,
            'items' => $data_penjualan

        ];
        echo json_encode($data);
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Penjualan_model->json();
    }

    public function read($id)
    {
        $row = $this->Penjualan_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'user' => $row->user,
                'total_penjualan' => $row->total_penjualan,
                'pelanggan' => $row->pelanggan,
                'created_at' => $row->created_at,
            );
            $this->load->view('penjualan/penjualan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('penjualan/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'user' => set_value('user'),
            'total_penjualan' => set_value('total_penjualan'),
            'pelanggan' => set_value('pelanggan'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('penjualan/penjualan_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'user' => $this->input->post('user', TRUE),
                'total_penjualan' => $this->input->post('total_penjualan', TRUE),
                'pelanggan' => $this->input->post('pelanggan', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Penjualan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penjualan'));
        }
    }

    public function update($id)
    {
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('penjualan/update_action'),
                'id' => set_value('id', $row->id),
                'kode' => set_value('kode', $row->kode),
                'user' => set_value('user', $row->user),
                'total_penjualan' => set_value('total_penjualan', $row->total_penjualan),
                'pelanggan' => set_value('pelanggan', $row->pelanggan),
                'created_at' => set_value('created_at', $row->created_at),
            );
            $this->load->view('penjualan/penjualan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'user' => $this->input->post('user', TRUE),
                'total_penjualan' => $this->input->post('total_penjualan', TRUE),
                'pelanggan' => $this->input->post('pelanggan', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Penjualan_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penjualan'));
        }
    }

    public function delete($id)
    {
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $this->Penjualan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penjualan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function _rules()
	{
		$this->form_validation->set_rules('kode', 'kode', 'trim');
		$this->form_validation->set_rules('user', 'user', 'trim');
		$this->form_validation->set_rules('total_penjualan', 'total_penjualan', 'trim');
		$this->form_validation->set_rules('pelanggan', 'pelanggan', 'trim');
	
		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

}

/* End of file Penjualan.php */
/* Location: ./application/controllers/Penjualan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */