<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Shop extends CI_Controller
{
	public $data_session;

	function __construct()
	{
		parent::__construct();
		$this->data_session = $this->session->userdata;

		// if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
		// 	redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		// }

		$this->load->model('Chat_model');
		$this->load->model('Payment_model');
		$this->load->model('Penjualan_detail_model');
		$this->load->model('Penjualan_model');
		$this->load->model('User_model');
		$this->load->model('Category_model');
		$this->load->model('Kurir_model');
		$this->load->model('Shop_model');
		$this->load->model('Barang_model');
		$this->load->model('Diskon_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$this->load->view('shop/main');
	}

	public function semua_produk()
	{
		$this->load->view('shop/all_category');
	}
	public function produk_bykategori($id)
	{
		$this->load->view('shop/by_category', ['data_id' => $id]);
	}

	public function chat(){
		$data_session = $this->session->userdata;
		$dataChat = $this->Chat_model->get_chat_from_admin($data_session['id']);
		$data['data_chat'] = $dataChat;

		$this->load->view('shop/chat',$data);
	}

	public function kirim_chat(){
		$data_session = $this->session->userdata;
		$id = $data_session['id'];
        $this->load->helper('form');
        $this->form_validation->set_rules('content', 'content', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('pesan', 'Pesan Masih Kosong');
           redirect(site_url('shop/chat'));
          
       } else {
           $data = array(
            'send_to' => 1,
            'send_by' => $id,
            'content' => $this->input->post('content', TRUE),
            'tanggal' => date("Y-m-d H:i:s"),
           );
           
           $this->Chat_model->insert($data);
           $this->User_model->update($id, ["recovery" =>1]);

           $this->session->set_flashdata('pesan', 'Pesan Terkirim');
          redirect(site_url('shop/chat'));
       }
    }

	public function cart()
	{
		// if (!$this->session->userdata('logged_in')) {
		// 	redirect(site_url('shop/masuk'));
		// }

		if (($this->session->userdata('logged_in'))) {
			$data_diskon = $this->Diskon_model->active_diskon();
			$data_detail_user = $this->User_model->get_detail_member_byid_kurir($this->data_session['id'])->get()->row();
		} else {
			$diskon_array = ["jumlah" => 0, "nama" => "-"];
			$data_diskon = (object) $diskon_array;

			$data_detail_user_array = ["nama_penerima" => "-", "harga" => "-", "alamat_penerima" => "-", "tlp" => "-"];
			$data_detail_user = (object) $data_detail_user_array;
		}

		$data_cart = $this->cart->contents();
		$load_data_cart = [];
		$final_data_cart = [];
		if ($data_cart) {


			foreach ($data_cart as $key) {

				$data_produk = $this->Barang_model->get_data_product_byid($key['id'])->get()->row();


				$load_data_cart['rowid'] = $key['rowid'];
				$load_data_cart['name'] = $key['name'];
				$load_data_cart['price'] = $key['price'];
				$load_data_cart['stock'] = $data_produk->stock;
				$load_data_cart['min_stock'] = $data_produk->min_stock;
				$load_data_cart['qty'] = $key['qty'];
				$load_data_cart['subtotal'] = $key['subtotal'];
				$load_data_cart['image'] = $data_produk->image;
				$load_data_cart['note'] = $data_produk->note;
				$load_data_cart['kode'] = $data_produk->kode;
				$load_data_cart['category_name'] = $data_produk->category_name;
				$load_data_cart['id_product'] = $key['id'];

				$final_data_cart[] = $load_data_cart;
			}
		}

		$data['data_kuir'] = $this->Kurir_model->get_all();
		$data['data_cart'] = $final_data_cart;
		$data['total_cart'] = $this->cart->total();
		$data['data_diskon'] = $data_diskon;
		$data['data_detail_user'] = $data_detail_user;


		//var_dump($data_diskon);
		$this->load->view('shop/cart', $data);
	}
	public function product_allcategory()
	{
		$data_all_product = $this->Barang_model->product_allcategory();
		$data = $data_all_product == null ? [] : $data_all_product;

		echo json_encode($data);
	}
	public function product_bycategory($id)
	{
		if ($id == "all") {
			$data_all_product = $this->Barang_model->product_allcategory();
			$data = $data_all_product == null ? [] : $data_all_product;
		} else {
			$data_all_product = $this->Barang_model->product_bycategory($id);
			$data = $data_all_product == null ? [] : $data_all_product;
		}


		echo json_encode($data);
	}
	public function produk_detail($id)
	{
		$data_produk = $this->Barang_model->get_data_product_byid($id)->get()->row();
		$recent_produk = $this->Barang_model->recent_produk($data_produk->category, $data_produk->id);
		//var_dump($data_produk);
		$this->load->view('shop/detail_produk', ['data_produk' => $data_produk, 'recent_produk' => $recent_produk]);
	}

	public function masuk()
	{
		$this->load->view('shop/login');
	}
	public function daftar()
	{
		$this->load->view('shop/registrasi');
	}
	public function lupa_password()
	{
		$this->load->view('shop/lupa_password');
	}

	public function pembayaran()
	{
		$this->load->view('shop/pembayaran');
	}
	public function pengiriman()
	{
		$this->load->view('shop/pengiriman');
	}
	public function tentangKami()
	{
		$this->load->view('shop/tentang_kami');
	}

	public function recovery($token)
	{
		$this->load->view('shop/recovery', ['token_recovery' => $token]);
	}

	public function member() // OVERVIEW
	{
		$data['data_member'] = $this->User_model->get_data_user_byid($this->data_session['id'])->get()->row();
		$data['recent_order'] = $this->Penjualan_model->recent_order($this->data_session['id']) == null ? [] : $this->Penjualan_model->recent_order($this->data_session['id']);
		$data['data_order'] = [
			'orders' => $this->Penjualan_model->penjualan_bymember($this->data_session['id']) == null ? 0 : count($this->Penjualan_model->penjualan_bymember($this->data_session['id'])),
			'processed_orders' => $this->Penjualan_model->recent_order_bystatus($this->data_session['id'], 2) == null ? 0 : count($this->Penjualan_model->recent_order_bystatus($this->data_session['id'], 2)),
			'delivered_orders' => $this->Penjualan_model->recent_order_bystatus($this->data_session['id'], 3) == null ? 0 : count($this->Penjualan_model->recent_order_bystatus($this->data_session['id'], 3))
		];

		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$data['main_content'] = 'shop/member_preview';
			$data['page_title'] = 'Halaman Preview Member';
			$this->load->view('shop/member', $data);
		}
	}

	public function order_list() // MY ORDER
	{

		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$data['main_content'] = 'shop/member_order_list';
			$data['page_title'] = 'Halaman List Order';
			$this->load->view('shop/member', $data);
		}
	}

	public function order_received() // HISTORY ORDER
	{
		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$data['main_content'] = 'shop/member_order_received';
			$data['page_title'] = 'Halaman Pesanan Sudah Di Terima';
			$this->load->view('shop/member', $data);
		}
	}

	public function order_shipped($id_transaksi)
    {
		$this->Penjualan_model->update_status($id_transaksi, ['status' => 3]);
		redirect(site_url('shop/member/order-list'));
		$this->session->set_flashdata('pesan', 'Terimaksih, Pesanan Sudah Di Terima');
    }

	public function checkout()
	{
		$data_cart = $this->cart->contents();

		if ($data_cart == null) { // JIKA KERANJANG KOSONG
			$this->session->set_flashdata('pesan', 'Keranjang Belanja Masih Kosong');
			redirect(site_url('shop/cart'));
		} else {

			if ($this->session->userdata('level') == 3) { //JIKA MEMBER

				$data_cart = $this->cart->contents();
				$data_session = $this->session->userdata;

				$data_user = $this->User_model->get_detail_member_byid_kurir($data_session['id'])->get()->row();

				$this->form_validation->set_rules('nama_penerima', 'nama_penerima',);
				$this->form_validation->set_rules('kurir', 'kurir',);
				$this->form_validation->set_rules('alamat_penerima', 'alamat_penerima',);
				$this->form_validation->set_rules('tlp', 'tlp',);

				if ($this->form_validation->run() == TRUE) {
					$this->session->set_flashdata('pesan', 'Gagal Checkout, Mohon Periksa Lagi');
					redirect(site_url('shop/cart'));
				} else {

					$active_diskon = $this->Diskon_model->active_diskon();



					$berat = 0;
					foreach ($data_cart as $key) {

						$berat += $this->Penjualan_model->get_weight_product($key['id']) * $key['qty']; // BERAT TOTAL

					}

					$berat_total = ceil($berat);
					$kurir = $this->Kurir_model->get_by_id($this->input->post('kurir', TRUE))->harga;
					$ongkir = $berat_total * $kurir; // ONGKIR

					//$total_penjualan = ($this->cart->total() / 100) * $active_diskon->jumlah; // INCLUDE DISKON

					// Insert ALamat
					$this->User_model->update_by_userid($data_session['id'],["alamat_penerima"=>$this->input->post('alamat_penerima', TRUE)]);
					$data_penjualan = array(
						'kode' => "TRX" . mt_rand(100000, 999999) . "-" . date('Ymd'),
						'user' => $data_session['id'],
						'diskon' => $active_diskon->id,
						'total_penjualan' => $this->cart->total(),
						'pelanggan' => 0,
						'status' => 0,
						'approvement' => 0,
						'kurir' => $this->input->post('kurir', TRUE),
						'ongkir' => $ongkir,
						'member' => "ya",
						'created_at' => date("Y-m-d"),
					);
					$idPenjualan = $this->Penjualan_model->insert_penjualan($data_penjualan);

					$data_payment = array(
						'penjualan' => $idPenjualan,
						'payment' => 0,
						'nama' => "-",
						'rekening' => 0,
						'foto' => "-"
					);
					$this->Penjualan_model->insert_detail_pembayaran($data_payment);

					foreach ($data_cart as $key) {
						$dataDetailTransaksi = [
							'barang' => $key['id'],
							'jumlah' => $key['qty'],
							'total' =>  $key['subtotal'],
							'penjualan' => $idPenjualan,
						];
						$this->Penjualan_detail_model->insert($dataDetailTransaksi);  // INSERT DETAIL TRANSAKSI
					}

					$this->cart->destroy();
					$kodeTransaksi = $this->Penjualan_model->get_by_id($idPenjualan)->kode;
					redirect(site_url('shop/invoice/') . $kodeTransaksi);
				}
			} else { //NON MEMEBER / DARI KASIR
				$data_cart = $this->cart->contents();

				$this->form_validation->set_rules('nama_penerima', 'nama_penerima',);
				$this->form_validation->set_rules('kurir', 'kurir',);
				$this->form_validation->set_rules('alamat_penerima', 'alamat_penerima',);
				$this->form_validation->set_rules('tlp', 'tlp',);

				if ($this->form_validation->run() == TRUE) {
					$this->session->set_flashdata('pesan', 'Gagal Checkout, Mohon Periksa Lagi');
					redirect(site_url('shop/cart'));
				} else {

					$berat = 0;
					foreach ($data_cart as $key) {

						$berat += $this->Penjualan_model->get_weight_product($key['id']) * $key['qty']; // BERAT TOTAL

					}

					$berat = ceil($berat);
					$kurir = $this->Kurir_model->get_by_id($this->input->post('kurir', TRUE))->harga;
					$ongkir = $berat * $kurir; // ONGKIR


					if ($this->session->userdata('logged_in')) {
						$data_pelanggan = array(
							'nama' => $this->input->post('nama_penerima', TRUE),
							'alamat_penerima' => "n/a",
							'tlp' => "n/a",
						);
					}else{
						$data_pelanggan = array(
							'nama' => $this->input->post('nama_penerima', TRUE),
							'alamat_penerima' => $this->input->post('alamat_penerima', TRUE),
							'tlp' => $this->input->post('tlp', TRUE),
						);
					}
					

					$idPelanggan = $this->Penjualan_model->insert_pelanggan($data_pelanggan);


					$data_penjualan = array(
						'kode' => "TRX" . mt_rand(100000, 999999) . "-" . date('Ymd'),
						'user' => 0,
						'diskon' => 0,
						'total_penjualan' => $this->cart->total(),
						'pelanggan' => $idPelanggan,
						'status' => $this->session->userdata('logged_in') ? 3 : 0,
						'approvement' => $this->session->userdata('logged_in') ? 1 : 0,
						'kurir' => $this->session->userdata('logged_in') ? 0 : $this->input->post('kurir', TRUE),
						'ongkir' => $ongkir,
						'member' => "tidak",
						'created_at' => date("Y-m-d"),
					);

					$idPenjualan = $this->Penjualan_model->insert_penjualan($data_penjualan);

					$data_payment = array(
						'penjualan' => $idPenjualan,
						'payment' => 0,
						'nama' => "-",
						'rekening' => 0,
						'foto' => "-"
					);
					$this->Penjualan_model->insert_detail_pembayaran($data_payment);

					foreach ($data_cart as $key) {
						$dataDetailTransaksi = [
							'barang' => $key['id'],
							'jumlah' => $key['qty'],
							'total' =>  $key['subtotal'],
							'penjualan' => $idPenjualan,
						];
						$this->Penjualan_detail_model->insert($dataDetailTransaksi);  // INSERT DETAIL TRANSAKSI
					}

					$this->cart->destroy();
					$kodeTransaksi = $this->Penjualan_model->get_by_id($idPenjualan)->kode;


					redirect(site_url('shop/invoice/') . $kodeTransaksi);


					// // $this->Kurir_model->insert($data);
					// // $this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
					// // redirect(site_url('master/kurir'));
				}
			}
		}
	}

	public function cari_invoice()
	{

		$id_transaksi = $this->input->post('cari', TRUE);

		$penjualan = $this->Penjualan_model->penjualan_by_idtransaksi($id_transaksi)->get()->row();

		if ($penjualan == null) {
			$this->session->set_flashdata('not-found', 'Data Tidak Ditemukan');
			redirect(site_url('shop/pembayaran'));
		} else {
			redirect(site_url('shop/invoice/') . $id_transaksi);
		}
	}

	public function invoice($id_transaksi)
	{
		$penjualan = $this->Penjualan_model->penjualan_by_idtransaksi($id_transaksi)->get()->row();
		$pembayaran  = $this->Payment_model->get_all();

		if ($penjualan == null) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('shop'));
		} else {
			if ($penjualan->member == "ya") { // PENJUALN SEBAGAI MEMBER

				$data_penjualan = $penjualan;
				$data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
				$data_penjualan_member = $this->Penjualan_model->penjualan_member($id_transaksi)->get()->row();
				$data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
				$data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
				$data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

				$data['data_penjualan'] = $data_penjualan;
				$data['data_penjualan_detail'] = $data_penjualan_detail;
				$data['data_penjualan_pelanggan'] = $data_penjualan_member;
				$data['data_penjualan_diskon'] = $data_penjualan_diskon;
				$data['data_penjualan_payment'] = $data_penjualan_payment;
			} else { // PENJUALN SEBAGAI PENGUNJUNG
				$data_penjualan = $penjualan;
				$data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
				$data_penjualan_pelanggan = $this->Penjualan_model->penjualan_pelanggan($id_transaksi)->get()->row();
				$data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
				$data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
				$data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

				$data['data_penjualan'] = $data_penjualan;
				$data['data_penjualan_detail'] = $data_penjualan_detail;
				$data['data_penjualan_pelanggan'] = $data_penjualan_pelanggan;
				$data['data_penjualan_diskon'] = $data_penjualan_diskon;
				$data['data_penjualan_kurir'] = $data_penjualan_kurir;
				$data['data_penjualan_payment'] = $data_penjualan_payment;
			}

			$data['data_metode_pembayaran'] = $pembayaran;
			$data['data_detail_pembayaran'] = $this->Penjualan_model->get_detail_pembayaran($penjualan->id)->get()->row();
			//var_dump($data);

			//print_r($data);
			$this->load->view('shop/invoice', $data);
		}
	}

	public function print_invoice($id_transaksi)
	{
		$penjualan = $this->Penjualan_model->penjualan_by_idtransaksi($id_transaksi)->get()->row();
		$pembayaran  = $this->Payment_model->get_all();

		if ($penjualan == null) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('shop'));
		} else {

			if ($penjualan->member == "ya") { // PENJUALN SEBAGAI MEMBER

				$data_penjualan = $penjualan;
				$data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
				$data_penjualan_member = $this->Penjualan_model->penjualan_member($id_transaksi)->get()->row();
				$data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
				$data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
				$data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

				$data['data_penjualan'] = $data_penjualan;
				$data['data_penjualan_detail'] = $data_penjualan_detail;
				$data['data_penjualan_pelanggan'] = $data_penjualan_member;
				$data['data_penjualan_diskon'] = $data_penjualan_diskon;
				$data['data_penjualan_payment'] = $data_penjualan_payment;
			} else { // PENJUALN SEBAGAI PENGUNJUNG
				$data_penjualan = $penjualan;
				$data_penjualan_detail = $this->Penjualan_model->penjualan_detail_by_idtransaksi($id_transaksi)->get()->result();
				$data_penjualan_pelanggan = $this->Penjualan_model->penjualan_pelanggan($id_transaksi)->get()->row();
				$data_penjualan_diskon = $this->Penjualan_model->penjualan_diskon($id_transaksi)->get()->row();
				$data_penjualan_kurir = $this->Penjualan_model->penjualan_kurir($id_transaksi)->get()->row();
				$data_penjualan_payment = $this->Penjualan_model->penjualan_payment($penjualan->id)->get()->row();

				$data['data_penjualan'] = $data_penjualan;
				$data['data_penjualan_detail'] = $data_penjualan_detail;
				$data['data_penjualan_pelanggan'] = $data_penjualan_pelanggan;
				$data['data_penjualan_diskon'] = $data_penjualan_diskon;
				$data['data_penjualan_kurir'] = $data_penjualan_kurir;
				$data['data_penjualan_payment'] = $data_penjualan_payment;
			}

			$data['data_metode_pembayaran'] = $pembayaran;
			$data['data_detail_pembayaran'] = $this->Penjualan_model->get_detail_pembayaran($penjualan->id)->get()->row();
			$data['image_logo'] = "assets/img/unmouth-rect.png";

			//var_dump($data);

			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "print_penjualan.pdf";
			$this->pdf->load_view('shop/print_nota', $data);
		}
	}

	public function kirim_bukti_pembayaran($id_transaksi)
	{

		$penjualan = $this->Penjualan_model->penjualan_by_idtransaksi($id_transaksi)->get()->row();

		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']  = '2048';
		$config['remove_space'] = TRUE;

		if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

			$this->load->library('upload', $config); // Load konfigurasi uploadnya

			if ($this->upload->do_upload('foto')) { // Lakukan upload dan Cek jika proses upload berhasil
				// Jika berhasil :
				$data = $this->upload->data();
				$message = $this->upload->data();

				$data_payment = array(
					'payment' => $this->input->post('payment', TRUE),
					'nama' => $this->input->post('nama', TRUE),
					'rekening' => $this->input->post('rekening', TRUE),
					'foto' => $data['file_name']
				);

				// UPDATE DATA
				$this->Penjualan_model->update_detail_pembayaran($penjualan->id, $data_payment);

				// UPDATE STATUS PENJUALAAN
				$this->Penjualan_model->update_status($penjualan->id, ['status' => 1]);
				$this->session->set_flashdata('pesan', 'Bukti pembayaran Sukses DI Simpan');
				redirect(site_url('shop/invoice/') . $penjualan->kode);
			} else {
				// Jika gagal :
				$data = ['file_name' => ''];
				$message = $this->upload->display_errors();
				redirect(site_url('shop/invoice/') . $penjualan->kode);
			}
		} else {
			$data_payment = array(
				'payment' => $this->input->post('payment', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'rekening' => $this->input->post('rekening', TRUE),
			);

			// UPDATE DATA
			$this->Penjualan_model->update_detail_pembayaran($penjualan->id, $data_payment);

			// UPDATE STATUS PENJUALAAN
			$this->Penjualan_model->update_status($penjualan->id, ['status' => 1]);

			$this->session->set_flashdata('pesan', 'Bukti pembayaran Sukses DI Simpan');
			redirect(site_url('shop/invoice/') . $penjualan->kode);
		}
	}



	public function setting() // STTING
	{

		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$data_member = $this->User_model->get_detail_member_byid($this->data_session['id'])->get()->row();
			$data['data_kuir'] = $this->Kurir_model->get_all();
			$data['data_member'] = $data_member;
			$data['main_content'] = 'shop/member_setting';
			$data['page_title'] = 'Halaman Pengatiran';
			$this->load->view('shop/member', $data);
		}
	}
	public function setting_action() // STTING
	{

		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$this->form_validation->set_rules('nama_penerima', 'nama_penerima', 'required');
			$this->form_validation->set_rules('alamat_penerima', 'alamat_penerima', 'required');
			$this->form_validation->set_rules('tlp', 'tlp', 'required');
			$this->form_validation->set_rules('kurir', 'kurir', 'required');

			if ($this->form_validation->run() == FALSE) {
				redirect('shop/member/setting'); // Cek udah login apa belum, kalo belum login dulu
				$this->session->set_flashdata('pesan', 'Gagal Mengubah Data');
			} else {

				$user = array(
					'tlp' => $this->input->post('tlp', TRUE),

				);
				$detail_user = array(
					'nama_penerima' => $this->input->post('nama_penerima', TRUE),
					'alamat_penerima' => $this->input->post('alamat_penerima', TRUE),
					'kurir' => $this->input->post('kurir', TRUE),
				);

				$this->User_model->update($this->data_session['id'], $user);
				$this->User_model->update_user_detail($this->data_session['id'], $detail_user);
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect(site_url('shop/member/setting'));
			}
		}
	}

	public function transaksi_json() // ORDER RUNNING
	{
		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {
			header('Content-Type: application/json');

			$dataPenjualan = $this->Penjualan_model->penjualan_bystatus_running_bymember($this->data_session['id']);

			$data['draw'] = 0;
			$data['recordsTotal'] = $dataPenjualan == null ? [] : count($dataPenjualan);
			$data['recordsFiltered'] = $dataPenjualan == null ? [] : count($dataPenjualan);
			$data['data'] = $dataPenjualan == null ? [] : $dataPenjualan;
			// var_dump($stockmup);
			echo json_encode($data);
		}
	}

	public function riwayat_transaksi_json() // ORDER SELESAI
	{
		header('Content-Type: application/json');

		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {


			$dataPenjualan = $this->Penjualan_model->recent_order_bystatus($this->session->userdata('id'), 3);

			$data['draw'] = 0;
			$data['recordsTotal'] = $dataPenjualan == null ? [] : count($dataPenjualan);
			$data['recordsFiltered'] = $dataPenjualan == null ? [] : count($dataPenjualan);
			$data['data'] = $dataPenjualan == null ? [] : $dataPenjualan;
			// var_dump($stockmup);
			echo json_encode($data);
		}
	}

	public function detail_transaksi($id_transaksi) // ORDER SELESAI
	{


		if ((!$this->session->userdata('logged_in')) || $this->data_session['level'] != 3) {
			redirect('shop/masuk'); // Cek udah login apa belum, kalo belum login dulu
			$this->session->set_flashdata('pesan', 'Silahkan Masuk Dulu');
		} else {

			$id = $this->Penjualan_model->get_penjualan_byuser_bykode($id_transaksi, $this->data_session['id'])->get()->row();
			if ($id == null) {
				redirect('/'); // Cek udah login apa belum, kalo belum login dulu
				$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			} else {
				$dataPenjualan = $this->Penjualan_model->get_penjualan_byuser($id->id, $this->data_session['id'])->get()->row();
				$dataDetailPenjualan = $this->Penjualan_model->get_detail_penjualan_byuser($id->id, $this->data_session['id'])->get()->result();

				$data['dataPenjualan'] = $dataPenjualan == null ? [] : $dataPenjualan;
				$data['dataDetailPenjualan'] = $dataDetailPenjualan == null ? [] : $dataDetailPenjualan;
				$this->load->view('shop/member_detail_transaksi', $data);
			}
		}
	}



	public function json()
	{
		header('Content-Type: application/json');
		echo $this->Category_model->json();
	}

	public function landing_page_data()
	{

		$data['data_top_produk'] =  $this->Shop_model->top_product();
		$data['data_new_produk'] =  $this->Shop_model->new_produk();
		$data['data_category'] =  $this->Shop_model->top_category();

		echo json_encode($data);
	}


	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
			redirect(site_url('master/category'));
		} else {
			$data = array(
				'nama' => $this->input->post('nama', TRUE),
			);

			$this->Category_model->insert($data);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('master/category'));
		}
	}

	public function edit($id)
	{
		$row = $this->Category_model->get_by_id($id);

		if ($row) {
			$data = array(
				'id' => set_value('id', $row->id),
				'nama' => set_value('nama', $row->nama),
				'main_content' => 'category/update',
				'page_title' => 'Edit Category'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('master/category'));
		}
	}

	public function update_action($id)
	{

		$category = $this->Category_model->get_by_id($id);
		$is_unique_name = $this->input->post('nama', TRUE) != $category->nama ? '|is_unique[category.nama]' : '';

		$this->form_validation->set_rules('nama', 'Nama', 'required' . $is_unique_name); //|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
			redirect(site_url('master/category'));
		} else {
			$data = array(
				'nama' => $this->input->post('nama', TRUE),
			);

			$this->Category_model->update($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('master/category'));
		}
	}

	public function delete($id)
	{
		$row = $this->Category_model->get_by_id($id);

		if ($row) {
			$this->Category_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('master/category'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('master/category'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama', 'nama', 'required|is_unique[category.nama]');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
