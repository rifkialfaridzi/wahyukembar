<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Supplier extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}
		$this->load->model('Supplier_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data['main_content'] = 'supplier/main';
		$data['page_title'] = 'Halaman Supplier';
		$this->load->view('template', $data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$supplier =  $this->Supplier_model->json();

		$data['draw'] = 0;
        $data['recordsTotal'] = $supplier == null ? [] : count($supplier);
        $data['recordsFiltered'] = $supplier == null ? [] : count($supplier);
        $data['data'] = $supplier == null ? [] : $supplier;
		
        echo json_encode($data);
	}

	public function read($id)
	{
		$row = $this->Supplier_model->get_by_id($id);
		if ($row) {
			$data = array(
				'id' => $row->id,
				'kode' => $row->kode,
				'nama' => $row->nama,
				'alamat' => $row->alamat,
				'no_tlp' => $row->no_tlp,
				'created_at' => $row->created_at,
			);
			$this->load->view('supplier/supplier_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('supplier'));
		}
	}

	public function create_action()
	{
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('pesan', 'Gagal Di Tambahkan');
            redirect(site_url('supplier'));

        } else {
           
		$data = array(
			'kode' => "SP".mt_rand(100000,999999),
			'nama' => $this->input->post('nama', TRUE),
			'alamat' => $this->input->post('alamat', TRUE),
			'no_tlp' => $this->input->post('tlp', TRUE),
			'created_at' => date("Y-m-d")
		);

		$this->Supplier_model->insert($data);
		$this->session->set_flashdata('pesan', 'Data Sukses Di Tambahkan');
		redirect(site_url('master/supplier'));
		
	}
}

	public function edit($id)
	{
		$row = $this->Supplier_model->get_by_id($id);

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('supplier/update_action'),
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'nama' => set_value('nama', $row->nama),
				'alamat' => set_value('alamat', $row->alamat),
				'no_tlp' => set_value('no_tlp', $row->no_tlp),
				'created_at' => set_value('created_at', $row->created_at),
				'main_content' => 'supplier/update',
				'page_title' => 'Edit Supplier'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('master/supplier'));
		}
	}

	public function update_action($id)
	{
		$supplier = $this->Supplier_model->get_by_id($id);
		$is_unique_name = $this->input->post('nama', TRUE) != $supplier->nama ? '|is_unique[supplier.nama]' : '';

		$this->form_validation->set_rules('nama', 'Nama', 'required'.$is_unique_name);//|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
			redirect(site_url('master/supplier'));
		} else {
			$data = array(
				
				'nama' => $this->input->post('nama', TRUE),
				'alamat' => $this->input->post('alamat', TRUE),
				'no_tlp' => $this->input->post('tlp', TRUE),

			);

			$this->Supplier_model->update($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('master/supplier'));
		}
	}

	public function delete($id)
	{
		$row = $this->Supplier_model->get_by_id($id);

		if ($row) {
			$this->Supplier_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Hapus');
			redirect(site_url('master/supplier'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('master/supplier'));
		}
	}

	public function _rules()
	{
		//$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'required|is_unique[supplier.nama]');
		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
		$this->form_validation->set_rules('tlp', 'tlp', 'trim|required');
		//$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel()
	{
		$this->load->helper('exportexcel');
		$namaFile = "supplier.xls";
		$judul = "supplier";
		$tablehead = 0;
		$tablebody = 1;
		$nourut = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=" . $namaFile . "");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Kode");
		xlsWriteLabel($tablehead, $kolomhead++, "Nama");
		xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
		xlsWriteLabel($tablehead, $kolomhead++, "No Tlp");
		xlsWriteLabel($tablehead, $kolomhead++, "Created At");

		foreach ($this->Supplier_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->kode);
			xlsWriteLabel($tablebody, $kolombody++, $data->nama);
			xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
			xlsWriteLabel($tablebody, $kolombody++, $data->no_tlp);
			xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}
}

/* End of file Supplier.php */
/* Location: ./application/controllers/Supplier.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:23 */
/* http://harviacode.com */
