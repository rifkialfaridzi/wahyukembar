-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2020 at 06:24 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `harga_grosir` varchar(225) NOT NULL,
  `min_grosir` varchar(225) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `harga_grosir`, `min_grosir`, `min_stock`, `note`, `created_at`) VALUES
(6, 'SKU0006', 'Kebaya Bali', 2, 6, '5', 'CONTENT7.jpg', '49', '50000', '0', '0', '5', 'Bahan monalisa wolfis yang lembut, berteksture polysonic, Adem dipakai\r\nAll size fit to XL LD 100 cm pjg baju 60- 65 cm pjg lengan 54cm\r\nBarang Produksi Sendiri !!\r\nReal Pict !! ', '2020-03-29'),
(7, 'SKU0007', 'Kebaya Muslim Kutubaru', 2, 1, '5', 'FROYO1.jpg', '10', '60000', '0', '0', '10', 'Bahan monalisa wolfis yang lembut, berteksture polysonic, Adem dipakai\r\nAll size fit to XL LD 100 cm pjg baju 60- 65 cm pjg lengan 54cm\r\nBarang Produksi Sendiri !!\r\nReal Pict !! ', '2020-04-03'),
(8, 'SKU0008', 'Kebaya Kutu Baru', 2, 5, '6', '1a.jpg', '18', '10000', '0', '0', '10', 'Bahan monalisa wolfis yang lembut, berteksture polysonic, Adem dipakai\r\nAll size fit to XL LD 100 cm pjg baju 60- 65 cm pjg lengan 54cm\r\nBarang Produksi Sendiri !!\r\nReal Pict !!', '2020-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(43, 'TR160604', 8, 8, '60000', '0', 1, '10', '2020-05-04'),
(44, 'TR772193', 6, 8, '35000', '0', 1, '8', '2020-05-04'),
(45, 'TR760449', 8, 8, '65000', '0', 1, '40', '2020-05-04'),
(46, 'TR454231', 6, 7, '35000', '0', 1, '50', '2020-05-04'),
(47, 'TR902036', 7, 2, '60000', '0', 1, '40', '2020-05-04'),
(48, 'TR386674', 7, 8, '50000', '0', 2, '50', '2020-05-09'),
(49, 'TR956570', 8, 8, '60000', '0', 1, '20', '2020-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(1, 'Baju Musilm'),
(4, 'Mukena Alabama'),
(5, 'Mukena Syari'),
(6, 'Celana Chinos');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(70, 'TRX531967-20200504', 1, '230000', '250000', '-20000', 'Pelanggan Terhormat', '2020-05-04 11:10:10'),
(71, 'TRX281866-20200504', 1, '230000', '250000', '-20000', 'Pelanggan Terhormat', '2020-05-04 06:39:39'),
(72, 'TRX898479-20200504', 1, '230000', '250000', '-20000', 'Pelanggan Terhormat', '2020-04-01 06:52:08'),
(73, 'TRX467010-20200504', 1, '230000', '250000', '-20000', 'Pelanggan Terhormat', '2020-05-04 07:23:21'),
(74, 'TRX251732-20200504', 1, '180000', '200000', '-20000', 'Pelanggan Terhormat', '2020-05-04 07:27:40'),
(75, 'TRX782581-20200509', 1, '650000', '700000', '-50000', 'Pelanggan Terhormat', '2020-05-09 03:01:12'),
(76, 'TRX793275-20200509', 1, '80000', '100000', '-20000', 'Pelanggan Terhormat', '2020-05-09 03:05:14'),
(77, 'TRX754059-20200509', 1, '10000', '10000', '0', 'Pelanggan Terhormat', '2020-05-09 11:09:06'),
(78, 'TRX667130-20200509', 1, '2150000', '2150000', '0', 'Pelanggan Terhormat', '2020-05-09 11:12:02'),
(79, 'TRX524504-20200509', 1, '1800000', '1800000', '0', 'Pelanggan Terhormat', '2020-05-09 11:12:56'),
(80, 'TRX971322-20200509', 1, '1140000', '1140000', '0', 'Pelanggan Terhormat', '2020-05-09 11:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `total`, `penjualan`) VALUES
(74, 8, '1', '100000', 70),
(75, 7, '1', '80000', 70),
(76, 6, '1', '50000', 70),
(77, 8, '1', '100000', 71),
(78, 7, '1', '80000', 71),
(79, 6, '1', '50000', 71),
(80, 8, '1', '100000', 72),
(81, 7, '1', '80000', 72),
(82, 6, '1', '50000', 72),
(83, 8, '1', '100000', 73),
(84, 6, '1', '50000', 73),
(85, 7, '1', '80000', 73),
(86, 8, '1', '100000', 74),
(87, 7, '1', '80000', 74),
(88, 8, '6', '600000', 75),
(89, 6, '1', '50000', 75),
(90, 7, '1', '80000', 76),
(91, 8, '1', '10000', 77),
(92, 8, '30', '300000', 78),
(93, 7, '30', '1800000', 78),
(94, 6, '1', '50000', 78),
(95, 7, '30', '1800000', 79),
(96, 8, '10', '100000', 80),
(97, 7, '9', '540000', 80),
(98, 6, '10', '500000', 80);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`) VALUES
(5, 'RAK0002', 'mawar'),
(6, 'RAK0003', 'melati1');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refund`
--

INSERT INTO `refund` (`id`, `kode`, `barang_masuk`, `jumlah`, `supplier`, `harga_pembelian`, `note`, `user`, `created_at`) VALUES
(24, 'RTR386674-090905052020', 48, '5', 8, '50000', 'xxxx', 1, '2020-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(121, 'STR772193-040405052020', 44, 6, 1, 8, '35000', '50000', 8, 0, 'Kondisi Ok', '400000', 0, '2020-05-04'),
(122, 'STR454231-040405052020', 46, 6, 1, 7, '35000', '50000', 50, 42, 'Kondisi Ok', '2500000', 0, '2020-05-04'),
(123, 'STR902036-040405052020', 47, 7, 1, 2, '60000', '80000', 40, 0, 'Kondisi Ok', '3200000', 0, '2020-05-04'),
(124, 'STR160604-040405052020', 43, 8, 1, 8, '60000', '100000', 10, 0, 'Kondisi Ok', '1000000', 0, '2020-05-04'),
(125, 'STR760449-040405052020', 45, 8, 1, 8, '65000', '100000', 40, 0, 'Kondisi Ok', '4000000', 0, '2020-05-04'),
(126, 'OTR807323', 43, 8, 0, 8, '60000', '100000', -1, -1, 'Barang Ok', '100000', 1, '2020-05-04'),
(127, 'OTR542270', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-04'),
(128, 'OTR788108', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-04'),
(129, 'OTR321756', 43, 8, 0, 8, '60000', '100000', -1, -1, 'Barang Ok', '100000', 1, '2020-05-04'),
(130, 'OTR273997', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-04'),
(131, 'OTR758053', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-04'),
(132, 'OTR794354', 43, 8, 0, 8, '60000', '100000', -1, -1, 'Barang Ok', '100000', 1, '2020-05-04'),
(133, 'OTR558228', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-04'),
(134, 'OTR918520', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-04'),
(135, 'OTR184684', 43, 8, 0, 8, '60000', '100000', -1, -1, 'Barang Ok', '100000', 1, '2020-05-04'),
(136, 'OTR447701', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-04'),
(137, 'OTR551352', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-04'),
(138, 'OTR173039', 43, 8, 0, 8, '60000', '100000', -1, -1, 'Barang Ok', '100000', 1, '2020-05-04'),
(139, 'OTR278612', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-04'),
(140, 'OTR519202', 43, 8, 0, 8, '60000', '100000', -5, -5, 'Barang Ok', '600000', 1, '2020-05-09'),
(141, 'OTR593211', 45, 8, 0, 8, '65000', '100000', -1, -1, 'Barang Ok', '600000', 1, '2020-05-09'),
(142, 'OTR859107', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-09'),
(143, 'OTR777094', 47, 7, 0, 2, '60000', '80000', -1, -1, 'Barang Ok', '80000', 1, '2020-05-09'),
(144, 'STR386674-090905052020', 48, 7, 1, 8, '50000', '60000', 45, 10, 'Kondisi Ok', '2700000', 0, '2020-05-09'),
(145, 'STR956570-090905052020', 49, 8, 1, 8, '60000', '10000', 20, 18, 'Kondisi Ok', '200000', 0, '2020-05-09'),
(146, 'OTR648576', 45, 8, 0, 8, '65000', '100000', -1, -1, 'Barang Ok', '10000', 1, '2020-05-09'),
(147, 'OTR722911', 45, 8, 0, 8, '65000', '100000', -30, -30, 'Barang Ok', '300000', 1, '2020-05-09'),
(148, 'OTR793440', 47, 7, 0, 2, '60000', '80000', -30, -30, 'Barang Ok', '1800000', 1, '2020-05-09'),
(149, 'OTR465481', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-09'),
(150, 'OTR987987', 47, 7, 0, 2, '60000', '80000', -4, -4, 'Barang Ok', '1800000', 1, '2020-05-09'),
(151, 'OTR483864', 48, 7, 0, 8, '50000', '60000', -26, -26, 'Barang Ok', '1800000', 1, '2020-05-09'),
(152, 'OTR402958', 45, 8, 0, 8, '65000', '100000', -8, -8, 'Barang Ok', '100000', 1, '2020-05-09'),
(153, 'OTR233296', 49, 8, 0, 8, '60000', '10000', -2, -2, 'Barang Ok', '100000', 1, '2020-05-09'),
(154, 'OTR720525', 48, 7, 0, 8, '50000', '60000', -9, -9, 'Barang Ok', '540000', 1, '2020-05-09'),
(155, 'OTR637886', 44, 6, 0, 8, '35000', '50000', -2, -2, 'Barang Ok', '500000', 1, '2020-05-09'),
(156, 'OTR785058', 46, 6, 0, 7, '35000', '50000', -8, -8, 'Barang Ok', '500000', 1, '2020-05-09');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(2, 'SP945722', 'Bambam', 'Grogol', '123012978', '2020-03-25'),
(4, 'SP556368', 'Welda', 'Sukoharjo', '9238748392', '2020-03-25'),
(5, 'SP975388', 'Namida', 'Konoha', '12345689', '2020-03-25'),
(7, 'SP420109', 'Grosir Batik Solo', 'PGS Blok S', '08137462373', '2020-03-25'),
(8, 'SP497763', 'Grosir Batik Grogol', 'Grogol', '081363599878', '2020-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(2, 'PCS'),
(9, 'Kilogram'),
(17, 'Seri'),
(18, 'Lusin'),
(19, 'Dosin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Samsudin', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Munaroh', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'maman', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
