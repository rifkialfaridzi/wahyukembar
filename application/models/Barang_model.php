<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_model extends CI_Model
{

    public $table = 'barang';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->db->select('barang.*,barang.kode as kode, barang.id as id, barang.image as image, barang.nama as nama, supplier.nama as supplier, category.nama as category, warna.nama as warna, bahan.nama as bahan');
        $this->db->from('barang');
        //add this line for join
        $this->db->join('category', 'barang.category = category.id');
        $this->db->join('supplier', 'barang.supplier = supplier.id');
        $this->db->join('warna', 'barang.warna = warna.id');
        $this->db->join('bahan', 'barang.bahan = bahan.id');
        // $this->datatables->add_column('action', anchor(site_url('barang/read/$1'),'Read')." | ".anchor(site_url('barang/update/$1'),'Update')." | ".anchor(site_url('barang/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->db->get()->result();
    }

    function product_allcategory(){

        $this->db->select('b.*,c.nama as category_name,ba.nama as bahan_name,w.nama as warna_name');
        $this->db->from('barang b');
        $this->db->join('category c', 'c.id=b.category', 'left');
        $this->db->join('warna w', 'w.id=b.warna', 'left');
        $this->db->join('bahan ba', 'ba.id=b.bahan', 'left');
        return $this->db->get()->result();
    }

    function product_bycategory($id_category){

        $this->db->select('b.*,c.nama as category_name');
        $this->db->from('barang b');
        $this->db->join('category c', 'c.id=b.category', 'left');
        return $this->db->where('b.category', $id_category)->get()->result();
    }
    function recent_produk($id_category,$ecept_id_produk){

        $this->db->select('b.*,c.nama as category_name,convert(b.harga_penjualan,decimal) as aa');
        $this->db->from('barang b');
        $this->db->join('category c', 'c.id=b.category', 'left');
        $this->db->where('b.id !=', $ecept_id_produk);
        return $this->db->where('b.category', $id_category)->order_by('aa','DESC')->limit(4)->get()->result();
    }
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function total_produk_bymonth($month)
    {
        $this->db->where('MONTH(created_at) >=', 1);
        $this->db->where('MONTH(created_at) <=', $month);
        $this->db->select('COUNT(id) as jumlah_produk, SUM(stock) as total_stock');
        return $this->db->from('barang')->get()->row();
    }

    // get data by id
    function get_bykategori($id)
    {
        $this->db->where('category', $id);
        return $this->db->get($this->table)->result();
    }

    function get_data_product_byid($id)
    {

        $this->db->select('b.*, c.nama as category_name, w.nama as warna_name, bn.nama as bahan_name');
        $this->db->from('barang b');
        $this->db->join('category c', 'c.id=b.category', 'left');
        $this->db->join('warna w', 'w.id=b.warna', 'left');
        $this->db->join('bahan bn', 'bn.id=b.bahan', 'left');
        return $this->db->where('b.id', $id);
    }

    function get_data_product_bykode($id)
    {

        $this->db->select('b.*, c.nama as category_name');
        $this->db->from('barang b');
        $this->db->join('category c', 'c.id=b.category', 'left');
        return $this->db->where('b.kode', $id);
    }
    
    function get_data_relational_byid($id)
    {

        $this->db->select('b.*, b.nama as barang_name, s.nama as supplier_name');
        $this->db->from('barang b');
        $this->db->join('stock st', 'st.barang=b.id', 'left');
        $this->db->join('barang_masuk bm', 'bm.barang=st.barang_masuk', 'left');
        $this->db->join('supplier s', 's.id=bm.supplier', 'left');
        return $this->db->where('st.barang', $id);
    }

    function get_data_relational_all()
    {

        $this->db->select('b.*, b.nama as barang_name, s.nama as supplier_name');
        $this->db->from('barang b');
        $this->db->join('stock st', 'st.barang=b.id', 'left');
        $this->db->join('barang_masuk bm', 'bm.barang=st.barang_masuk', 'left');
        return $this->db->join('supplier s', 's.id=bm.supplier', 'left');
    }



    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // UPDATE STOCK
    function update_stock($id, $data)
    {
        $stock = $this->get_by_id($id)->stock;
        $stock_sisa = $stock - $data;
        
        $this->db->where($this->id, $id);
        $this->db->update($this->table, ['stock' => $stock_sisa]);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Barang_model.php */
/* Location: ./application/models/Barang_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:19 */
/* http://harviacode.com */
