<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{

    public $table = 'penjualan';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->datatables->select('id,kode,user,total_penjualan,pelanggan,created_at');
        $this->datatables->from('penjualan');
        //add this line for join
        //$this->datatables->join('table2', 'penjualan.field = table2.field');
        ///$this->datatables->add_column('action', anchor(site_url('penjualan/read/$1'),'Read')." | ".anchor(site_url('penjualan/update/$1'),'Update')." | ".anchor(site_url('penjualan/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function transaksi_bymonth($month)
    {
        $this->datatables->select('id,kode,user,total_penjualan,pelanggan,created_at');
        $this->datatables->from('penjualan');
        $this->datatables->where('MONTH(created_at)', $month);
        //add this line for join
        //$this->datatables->join('table2', 'penjualan.field = table2.field');
        ///$this->datatables->add_column('action', anchor(site_url('penjualan/read/$1'),'Read')." | ".anchor(site_url('penjualan/update/$1'),'Update')." | ".anchor(site_url('penjualan/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function insert_pelanggan($data) // TRANSAKSI CHECKOUT NONMEMBER
    {
        $this->db->trans_start();
        $this->db->insert('pelanggan', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }
    function insert_penjualan($data)
    {
        $this->db->trans_start();
        $this->db->insert('penjualan', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    // AMBIL DATA USER -> MEMMBER / NON MEMBER
    function approvement_byid($user) // ADMIN APPROVMENT
    {
        $this->db->select('name');
        $this->db->from('user');
        return $this->db->where('id', $user);
    }

    function member_byid($user)
    {
        $this->db->select('ud.alamat_penerima as alamat_penerima, ud.nama_penerima as nama, u.tlp as tlp');
        $this->db->from('user u');
        $this->db->join('user_detail ud', 'ud.user=u.id', 'left');
        return $this->db->where('u.id', $user);
    }

    function non_member_byid($user)
    {
        $this->db->select('*');
        $this->db->from('pelanggan');
        return $this->db->where('id', $user);
    }
    // AMBIL DATA USER -> MEMMBER / NON MEMBER

    // KIRIM BUKTI PEMBAYARAN
    function insert_detail_pembayaran($data)
    {
        $this->db->trans_start();
        $this->db->insert('detail_payment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }
    function update_detail_pembayaran($id,$data)
    {
        $this->db->where('penjualan', $id);
        $this->db->update('detail_payment', $data);
    }
    function get_detail_pembayaran($id_pembayaran)
    {
        $this->db->select('*');
        $this->db->from('detail_payment');
        return $this->db->where('penjualan', $id_pembayaran);
    }
     // KIRIM BUKTI PEMBAYARAN

    // UPDATE STATUS PENJUALAN 
    function update_status($id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update('penjualan', $data);
    }

    // INVOICE -- 

    function get_weight_product($id_product)
    {

        $this->db->select('berat');
        $this->db->from('barang');
        return $this->db->where('id', $id_product)->get()->row()->berat;
    }

    function penjualan_by_idtransaksi($id_transaksi)
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        return $this->db->where('p.kode', $id_transaksi);
    }

    function penjualan_detail($id_transaksi)
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');

        return $this->db->where('p.id', $id_transaksi);
    }

    function penjualan_detail_by_idtransaksi($id_transaksi)
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');

        return $this->db->where('p.kode', $id_transaksi);
    }

    function penjualan_member($id_transaksi) /// MEMBER
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        $this->db->join('user u', 'u.id=p.user', 'left');
        $this->db->join('user_detail ud', 'ud.user=u.id', 'left');

        return $this->db->where('p.kode', $id_transaksi);
    }

    function penjualan_pelanggan($id_transaksi) /// NON MEMBER
    {

        $this->db->select('pl.*');
        $this->db->from('penjualan p');
        $this->db->join('pelanggan pl', 'pl.id=p.pelanggan', 'left');

        return $this->db->where('p.kode', $id_transaksi);
    }

    function penjualan_diskon($id_transaksi) /// NON MEMBER
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        $this->db->join('diskon d', 'd.id=p.diskon', 'left');

        return $this->db->where('p.kode', $id_transaksi);
    }

    function penjualan_kurir($id_transaksi) /// NON MEMBER
    {

        $this->db->select('*');
        $this->db->from('penjualan p');
        $this->db->join('kurir k', 'k.id=p.kurir', 'left');

        return $this->db->where('p.kode', $id_transaksi);
    }
    function penjualan_payment($id_transaksi) /// NON MEMBER
    {

        $this->db->select('py.*,dp.rekening as rekening');
        $this->db->from('detail_payment dp');
        $this->db->join('payment py', 'py.id=dp.payment', 'left');

        return $this->db->where('dp.penjualan', $id_transaksi);
    }




    // function data_invoice($id_transaksi)
    // {

    //     $penjualan = $this->db->where('kode', $id_transaksi)->get('penjualan')->row();

    //     if ($penjualan->user == 0) {
    //         return "non member";
    //     } else {
    //         $this->db->select('p.*');
    //         $this->db->from('penjualan p');
    //         $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
    //         $this->db->join('barang b', 'b.id=pd.barang', 'left');
    //         $this->db->join('user u', 'u.id=p.user', 'left');
    //         $this->db->join('user_detail ud', 'ud.user=u.id', 'left');
    //         return $this->db->where('p.kode', $id_transaksi);
    //     }


    //     // $this->db->select('*');
    //     // $this->db->from('penjualan p'); 
    //     // $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
    //     // $this->db->join('barang b', 'b.id=pd.barang', 'left');



    // }
    // INVOICE ---- 
    function semua_penjualan_print($date,$status)
    {

        
        $this->db->select('*,(CASE status WHEN 1 THEN 1 WHEN 0 THEN 2 WHEN 2 THEN 3 WHEN 3 THEN 4 ELSE 5 END) as available_status');

        if ($date != null) {

            $dateStart = $date['dateStart'];
            $dateEnd = $date['dateEnd'];

            $this->db->where('DATE(created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        }

        if ($status != "all") {
            $this->db->where('status',$status);
        }
    
        $this->db->order_by('id','desc');
        return $this->db->from('penjualan');

    }

    function semua_penjualan($date,$status)
    {

        
        $this->db->select('*,(CASE status WHEN 1 THEN 1 WHEN 0 THEN 2 WHEN 2 THEN 3 WHEN 3 THEN 4 ELSE 5 END) as available_status');

        if ($date != null) {

            $dateStart = $date['dateStart'];
            $dateEnd = $date['dateEnd'];

            $this->db->where('DATE(created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        }

        if ($status != "all") {
            $this->db->where('status',$status);
        }
       
        $this->db->order_by('available_status','asc');
        $this->db->order_by('id','desc');
        return $this->db->from('penjualan');

    }

    function total_order_bymonth($month)
    {
        $this->db->where('MONTH(created_at)', $month);
        $this->db->where('status', 3);
        return $this->db->get($this->table)->result();
    }

    function penjualan_range($dateStart, $dateEnd)
    {

        $this->db->where('DATE(p.created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('p.*,SUM(pd.jumlah) as jumlah_items');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->group_by('pd.penjualan');
        return $this->db->from('penjualan p');
    }

    function produk_order_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->where('status', 3);
        $this->db->select('SUM(pd.jumlah) as total_produk, SUM(p.total_penjualan) as total_penjualan');
        $this->db->from('penjualan p');
        return $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left')->get()->row();
    }

    function pendapatan_order_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->where('status', 3);
        $this->db->select('SUM(p.total_penjualan) as total_penjualan');
        
        return $this->db->from('penjualan p')->get()->row();
    }

    function jumlah_produk_order_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->where('status', 3);
        $this->db->select('SUM(pd.jumlah) as total_produk');
        $this->db->from('penjualan p');
        return $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left')->get()->row();
    }

    function top_product_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->where('p.status', 3);
        $this->db->select('SUM(pd.jumlah) as total_produk, b.nama as nama_produk, b.image as image,  b.kode as kode');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');
        return $this->db->group_by('pd.barang')->order_by('total_produk','desc')->limit('5')->get()->result();
    }

    function recent_order($id_member)
    {
        $this->db->where('p.user', $id_member);
        $this->db->select('p.*,pd.jumlah as jumlah, pd.total as total, b.nama as nama_barang,b.kode as kode_barang,b.image as image');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        return $this->db->join('barang b', 'b.id=pd.barang', 'left')->order_by('p.created_at', 'DESC')->limit('6')->get()->result();
    }

    function recent_order_bystatus($id_member, $status)
    {
        $this->db->where('user', $id_member);
        $this->db->where('status', $status);
        $this->db->select('*');
        return $this->db->from('penjualan')->order_by('created_at', 'DESC')->get()->result();
    }

    function penjualan_bymember($id_member)
    {
        $this->db->where('user', $id_member);
        $this->db->select('*');
        return $this->db->from('penjualan')->get()->result();
    }
    function penjualan_bystatus_running_bymember($id_member)
    {

        $this->db->group_start(); //this will start grouping
        $this->db->or_where('status', 1);
        $this->db->or_where('status', 2);
        $this->db->or_where('status', 0);
        $this->db->or_where('status', 4);
        $this->db->group_end(); //this will end grouping
        $this->db->where('user', $id_member);
        $this->db->select('*');
        return $this->db->from('penjualan')->get()->result();
    }

    function penjualan_terakhir()
    {
        $this->db->select('p.*');
        $this->db->from('penjualan p');
        $this->db->order_by("id", "desc");
        // $this->db->join('barang b', 'b.id=pd.barang', 'left');
        return $this->db->limit('10')->get()->result();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_data_product_byid($id)
    {

        $this->db->select('p.*, pd.jumlah as pd_jumlah, pd.total as pd_total, b.nama as nama_barang, b.kode as kode_barang, b.harga_penjualan as harga_barang');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');

        return $this->db->where('p.id', $id);
    }

    function get_penjualan_byuser($id, $id_member)
    {

        $this->db->select('p.*');
        $this->db->from('penjualan p');
        $this->db->where('p.user', $id_member);
        return $this->db->where('p.id', $id);
    }

    function get_penjualan_byuser_bykode($kode, $id_member)
    {

        $this->db->select('p.*');
        $this->db->from('penjualan p');
        $this->db->where('p.user', $id_member);
        return $this->db->where('p.kode', $kode);
    }

    function get_detail_penjualan_byuser($id, $id_member)
    {

        $this->db->select('p.*, pd.jumlah as pd_jumlah, pd.total as pd_total, b.nama as nama_barang, b.kode as kode_barang, b.harga_penjualan as harga_barang,b.image as image');
        $this->db->from('penjualan p');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');
        $this->db->where('p.user', $id_member);
        return $this->db->where('p.id', $id);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('user', $q);
        $this->db->or_like('total_penjualan', $q);
        $this->db->or_like('pelanggan', $q);
        $this->db->or_like('created_at', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('user', $q);
        $this->db->or_like('total_penjualan', $q);
        $this->db->or_like('pelanggan', $q);
        $this->db->or_like('created_at', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Penjualan_model.php */
/* Location: ./application/models/Penjualan_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
