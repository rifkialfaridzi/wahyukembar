<section ng-app="myApp" ng-controller="someController" class="section">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-stats">
          <div class="card-stats-title">Order Statistics -
            <div class="dropdown d-inline">
              <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">Pilih Bulan</a>
              <ul class="dropdown-menu dropdown-menu-sm">
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(1)">January</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(2)">February</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(3)">March</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(4)">April</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(5)">May</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(6)">June</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(7)">July</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(8)">August</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(9)">September</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(10)">October</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(11)">November</a></li>
                <li><a href="#" class="dropdown-item" ng-click="selectMonth(12)">December</a></li>
              </ul>
            </div>
          </div>
          <div class="card-stats-items">
            <div class="card-stats-item">
              <div class="card-stats-item-count">{{totalProduk}}</div>
              <div class="card-stats-item-label">Jumlah Produk</div>
            </div>
            <div class="card-stats-item">
              <div class="card-stats-item-count">{{totalSupplier}}</div>
              <div class="card-stats-item-label">Jumlah Supplier</div>
            </div>
            <div class="card-stats-item">
              <div class="card-stats-item-count">{{totalProdukOrder}}</div>
              <div class="card-stats-item-label">Produk Terjual</div>
            </div>
          </div>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-archive"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Transaksi</h4>
          </div>
          <div class="card-body">
            {{ totalOrder  }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-dollar-sign"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pendapatan</h4>
          </div>
          <div class="card-body">
            {{totalPendapatan | currency : "Rp."}}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <h4>Penjualan Terakhir</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>Invoice ID</th>
                <th>Pelanggan</th>
                <th>Admin</th>
                <th>Tagihan</th>
                <th>Status</th>
                <th>Tanggal</th>
                <th>Action</th>
              </tr>
              <?php foreach ($transaksi_terakhir as $key) {
                # code...
              ?>
                <tr>
                  <td><a target="_blank" href="<?php echo base_url('shop/invoice/' . $key->kode); ?>"><?php echo $key->kode; ?></a></td>
                  <td class="font-weight-600"><?php echo $key->name_user_order; ?></td>
                  <td class="font-weight-600"><?php echo $key->admin_name; ?></td>
                  <td class="font-weight-600">
                    <div class="badge badge-primary"> Rp. <?php echo number_format($key->total_penjualan, 2, ",", "."); ?></div>
                  </td>
                  <td class="font-weight-600">
                    <div class="badge badge-primary"><?php echo $key->order_status; ?></div>
                  </td>
                  <td><?php $time = strtotime($key->created_at);
                      echo date('j F, Y', $time); ?></td>
                  <td>
                    <a target="_blank" href="<?php echo base_url('shop/invoice/' . $key->kode); ?>" class="btn btn-primary">Detail</a>
                  </td>
                </tr>
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card gradient-bottom">
        <div class="card-header">
          <h4>Top 5 Produk</h4>
          <div class="card-header-action">
            <a id="cetak_produks" target="_blank" class="btn btn-icon btn-info" href="<?php echo base_url("admin/print_produk_terlaris/"). date('m')?>"><i class="fas fa-print"></i> Cetak</a>
          </div>
        </div>
        <div class="card-body" id="top-5-scroll">
          <ul class="list-unstyled list-unstyled-border">
            <li class="media" ng-repeat="product in topProduk">
              <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/uploads/'); ?>{{ product.image }}" alt="product">
              <div class="media-body">
                <div class="float-right">
                  <div class="font-weight-600 text-muted text-small">{{product.total_produk}} Terjual</div>
                </div>
                <div class="media-title">{{product.nama_produk}}</div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-footer pt-3 d-flex justify-content-center">
          <p>Produk Terlaris</p>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  var app = angular.module('myApp', ['ui.bootstrap']);

  app.controller('someController', function($scope, $filter, $http) {



    $scope.selectMonth = function(month) {
      $http.get('<?php echo base_url("admin/dashboard/"); ?>' + month, {
        msg: 'hello word!'
      }).
      then(function(response) {
        $scope.totalSupplier = response.data.supplier;
        $scope.totalOrder = response.data.totalOrder;
        $scope.totalProdukOrder = response.data.totalProdukOrder;
        $scope.totalPendapatan = response.data.totalPendapatan;
        $scope.topProduk = response.data.topProduk;
        $scope.totalProduk = response.data.produk.jumlah_produk;

        console.log(response.data);
      }, function(response) {
        console.log('error bos');
      });

      $("#cetak_produks").attr("href", "<?php echo base_url("admin/print_produk_terlaris/")?>"+month)

    }

    var date = new Date();
    $scope.selectMonth(date.getMonth() + 1);
   




  });
</script>