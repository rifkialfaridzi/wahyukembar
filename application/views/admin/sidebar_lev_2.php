<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <br><img src="<?php echo base_url('assets/img/unmouth.png'); ?>" alt="logo" width="200"><br><br>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">UNM</a>
    </div>
    <ul class="sidebar-menu">
        <!--
        <li class="menu-header">Starter</li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Layout</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Default Layout</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Utilities</span></a>
            <ul class="dropdown-menu">
                <li><a href="utilities-contact.html">Contact</a></li>
                <li><a class="nav-link" href="utilities-invoice.html">Invoice</a></li>
                <li><a href="utilities-subscribe.html">Subscribe</a></li>
            </ul>
        </li>
-->
        <li <?php if ($this->uri->segment(1) == "admin") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("admin"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        <li <?php if ($this->uri->segment(1) == "produk") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url(); ?>"><i class="fas fa-cart-plus"></i> <span>Produk</span></a></li>
        <li <?php if ($this->uri->segment(1) == "penjualan") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("penjualan"); ?>"><i class="fas fa-shopping-cart"></i> <span>Penjualan</span></a></li>
        <li <?php if ($this->uri->segment(1) == "diskon") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("diskon"); ?>"><i class="fas fa-percent"></i><span>Diskon</span></a></li>
        <li <?php if ($this->uri->segment(1) == "member") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("member"); ?>"><i class="fas fa-user"></i> <span>Member</span></a></li>
    </ul>


</aside>