<div style="text-align:center">
<div><img style="width: auto;height:100px" src="<?php echo base_url('assets/img/unmouth-rect.png') ?>"></img></div> <br>
    <h2 style="text-align:center"><strong>Laporan Stock Produk</strong></h2>
    <p>Di Cetak Pada: <?php echo date("d M Y"); ?></p>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">

        <tbody>
            <tr>
                <td style="text-align:center">Kode</td>
                <td style="text-align:center">Nama</td>
                <td style="text-align:center">Ukuran</td>
                <td style="text-align:center">Bahan</td>
                <td style="text-align:center">Warna</td>
                <td style="text-align:center">Harga Penjualan</td>
                <td style="text-align:center">Stock</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                    <td style="text-align:center"><?php echo $key->kode; ?></td>
                    <td style="text-align:center"><?php echo $key->nama; ?></td>
                    <td style="text-align:center"><?php echo $key->ukuran; ?></td>
                    <td style="text-align:center"><?php echo $key->bahan_name; ?></td>
                    <td style="text-align:center"><?php echo $key->warna_name; ?></td>
                    <td style="text-align:center">Rp. <?php echo number_format($key->harga_penjualan); ?></td>
                    <td style="text-align:center"><?php echo $key->stock; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $username; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Batik Wahyu Kembar</td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>