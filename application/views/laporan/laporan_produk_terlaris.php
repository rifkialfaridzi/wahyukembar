<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Produk Terlaris</title>
</head>
<body>
<div style="text-align:center">
    <div><img style="width: auto;height:100px" src="<?php echo base_url('assets/img/unmouth-rect.png') ?>"></img></div> <br>
    <h2 style="text-align:center"><strong>Laporan Produk Terlaris</strong></h2>

    <h5 style="text-align:center"></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:center">Kode</td>
                <td style="text-align:center">Nama Produk</td>
                <td style="text-align:center">Jumlah Terjual</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                    <td style="text-align:center"><?php echo $key->kode; ?></td>
                    <td style="text-align:center"><?php echo $key->nama_produk; ?></td>
                    <td style="text-align:center"><?php echo $key->total_produk; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong>Pimpinan</strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Batik Wahyu Kembar</td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>
</body>
</html>