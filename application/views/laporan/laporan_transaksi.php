<div style="text-align:center">
<div><img style="width: auto;height:100px" src="<?php echo base_url('assets/img/unmouth-rect.png') ?>"></img></div> <br>
    <h2 style="text-align:center"><strong>Laporan Penjualan</strong></h2>

    <h5 style="text-align:center">Tanggal <?php echo $date['dateStart']; ?> s/d&nbsp; <?php echo $date['dateEnd']; ?></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
            <td style="text-align:center">Kode Transaksi</td>
            <td style="text-align:left">Pelanggan</td>
                <td style="text-align:left">Status</td>
                <td style="text-align:center">Total Penjualan</td>
                <td style="text-align:center">Tanggal</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                <td style="text-align:center"><?php echo $key->kode; ?></td>
                <td style="text-align:left"><?php echo $key->nama_penerima; ?></td>
                    <td style="text-align:left"><?php echo $key->order_status; ?></td>
                    <td style="text-align:center">Rp. <?php echo number_format($key->total_penjualan,2,',','.'); ?></td>
                    <td style="text-align:center"><?php echo $key->created_at; ?></td>
                </tr>
            <?php } ?>
            <tr style="border: none;">
            <td style="text-align: right;padding: 15px;border: none;"><b>Total:</b></td>
            <td colspan="4" style="text-align: center;padding: 15px;border: none;"><b>Rp. <?php echo number_format($omset,2,',','.'); ?></b></td>
            
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $username; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Batik Wahyu Kembar</td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>