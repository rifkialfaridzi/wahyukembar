<section class="section">
    <div class="section-header">
        <h1>Halaman Member</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div id="reset-password" class="card">
                    <div class="card-header">
                        <h4>Permintaan Atur Ulang Password</h4>
                        <div class="card-header-action">
                            <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="collapse show" id="mycard-collapse">
                        <div class="card-body">
                            <form method="POST" action="<?php echo base_url("member/recovery_password"); ?>" class="needs-validation" novalidate="">
                                <div class="form-group">
                                    <label for="nama">Nomor Whatsapp</label>
                                    <input id="tlp" type="number" class="form-control" name="tlp" value="<?php echo $data_user->tlp; ?>" readonly  tabindex="1" required autofocus>
                                    <div class="invalid-feedback">
                                    Nomor Whatsapp Masih Kosong
                                    </div>
                                </div>

                                <div class="form-group">
                                    <a href="https://api.whatsapp.com/send?phone=<?php echo $data_user->tlp; ?>&text=Halo%20<?php echo $data_user->name; ?>%20Klik%20tautan%20berikut%20untuk%20mengatur%20ulang kata%20sandi%20<?php echo base_url('shop/recovery/').$data_user->token_verifikasi; ?>" target="_blank" class="btn btn-danger btn-lg btn-block" tabindex="4">
                                        Reset Ulang Password
                                    </a>
                                </div>
                            </form>

                            <?php if ($this->session->flashdata('pesan')) { ?>
                                <div class="alert alert-warning alert-dismissible show fade">
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <?php echo $this->session->flashdata('pesan');
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <br>               
                <div class="card profile-widget">
                    <div class="profile-widget-header">
                        <img alt="image" src="<?php echo base_url('assets/img/avatar/avatar-1.png'); ?>" class="rounded-circle profile-widget-picture">
                        <div class="profile-widget-items">
                            <div class="profile-widget-item">
                                <div class="profile-widget-item-label">Bergabung</div>
                                <div class="profile-widget-item-value"><?php echo $data_user->created_at == null ? 0 : $data_user->created_at; ?></div>
                            </div>
                            <div class="profile-widget-item">
                                <div class="profile-widget-item-label">Jumlah Transaksi</div>
                                <div class="profile-widget-item-value"><?php echo $jumlah_transaksi == null ? 0 : $jumlah_transaksi; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-widget-description pb-0">
                        <div class="profile-widget-name"><?php echo $data_user->name; ?> <div class="text-muted d-inline font-weight-normal">
                                <div class="slash"></div> <?php echo $data_user->tlp; ?>
                            </div>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Username
                                <span class="badge badge-primary badge-pill"><?php echo $data_user->username; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Email
                                <span class="badge badge-primary badge-pill"><?php echo $data_user->email; ?></span>
                            </li>
                        </ul>
                        <br>
                        <div class="form-group">
                            <label>Alamat Penerima</label>
                            <textarea class="form-control" readonly style="min-height: 150px"><?php echo $data_user->alamat_penerima; ?></textarea>
                        </div>

                    </div>
                </div>

                <div class="card chat-box card-success" id="mychatbox2">
                  <div class="card-header">
                    <h4><i class="fas fa-circle text-success mr-2" title="Online" data-toggle="tooltip"></i> Chat dengan <?php echo $data_user->name; ?></h4>
                  </div>
                  <div class="card-body chat-content">

                    <?php foreach ($data_chat as $data_chats) { ?>
                       
                        <?php if ($data_chats->send_by == 1) { ?>
                             <div class="chat-item chat-right" style=""><img src="<?php echo base_url("/assets/img/avatar/avatar-2.png"); ?>"><div class="chat-details"><div class="chat-text"><?php echo $data_chats->content;?></div><div class="chat-time"><?php echo $data_chats->tanggal;?></div></div></div>
                        <?php }else{ ?>
                            <div class="chat-item chat-left" style=""><img src="<?php echo base_url("/assets/img/avatar/avatar-5.png"); ?>"><div class="chat-details"><div class="chat-text"><?php echo $data_chats->content;?></div><div class="chat-time"><?php echo $data_chats->tanggal;?></div></div></div>
                        <?php } ?>
                    <?php } ?>

                   
                  </div>
                  <div class="card-footer chat-form">
                    <form method="POST" action="<?php echo base_url("member/kirim_chat/").$data_user->id; ?>" class="needs-validation" novalidate="" id="chat-form2">
                      <input type="text" name="content" class="form-control" placeholder="Type a message">
                      <button class="btn btn-primary">
                        <i class="far fa-paper-plane"></i>
                      </button>
                    </form>
                  </div>
                </div>

            </div>
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Riwayat Transaksi</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="category_tabel" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            #Kode
                                        </th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin ?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {
        //datatables
        $("#reset-password").hide();
        if (<?php echo $data_user->recovery; ?> == 1) {
            $("#reset-password").show();
        }
        
        table = $('#category_tabel').DataTable({
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": '<?php echo site_url('member/transaksi_json/').$data_user->id; ?>',
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columns": [{
                    "data": "kode"
                },
                {
                    "data": "total_penjualan"
                },
                {
					"data": null,
					"render": function(data, type, row) {
						if (row.status == 0) {
							return ' <div class="badge badge-danger">Belum Di Bayar</div>';
						} else if (row.status == 1) {
							return ' <div class="badge badge-primary">Sudah Di Bayar</div>';
						} else {
							return ' <div class="badge badge-success">Selesai</div>';
						}

					}
				},
                {
                    "data": "created_at"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<a href="<?php echo site_url("master/penjualan/edit/") ?>' + row.kode + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle"></i></a>';
                    }
                }
            ],

        });



    });
</script>