<section class="section">
	<div class="section-header">
		<h1>Halaman Member</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Member</h4>

						<div class="card-header-action">
							<a id="cetak" target="_blank" href="<?php echo base_url('member/print_member'); ?>" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="category_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>
											#Kode
										</th>
										<th>Nama Lengkap</th>
										<th>Username</th>
										<th>Email</th>
										<th>No. Telp</th>
										<th>Request</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Member</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#category_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('member/member_json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "id"
				},
				{
					"data": "name"
				},
				{
					"data": "username"
				},
				{
					"data": "email"
				},
				{
					"data": "tlp"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (parseInt(row.recovery) == 1) {
							return ' <a href="#" class="btn btn-icon btn-warning" title="Butuh Bantuan"><i class="fas fa-exclamation-triangle"></i></a>';
						} else {
							return ' <a href="#" class="btn btn-icon btn-success" title="Tidak Butuh Bantuan"><i class="fas fa-check"></i></a>';
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("member/detail/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("member/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}

	$(function() {
		var txt = $("#username");
		var func = function() {
			txt.val(txt.val().replace(/\s/g, ''));
			txt.val(txt.val().toLowerCase());
		}
		txt.keyup(func).blur(func);
	});
</script>