<section class="section">
	<div class="section-header">
		<h1>Halaman Penjualan</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Penjualan</h4>
					</div>
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-4">
								<label>Pilih Status</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">
											<i class="fas fa-star"></i>
										</div>
									</div>
									<select id="select_status" class="form-control form-control-lg">
										<option value="all">Semua</option>
										<option value="0">Belum Dibayar</option>
										<option value="1">Sudah Dibayar</option>
										<option value="2">Dikirim</option>
										<option value="3">Selesai</option>
										<option value="4">Ditolak</option>
									</select>
								</div>
							</div>
							<div class="form-group col-8">
								<label>Pilih Tanggal</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">
											<i class="fas fa-calendar"></i>
										</div>
									</div>
									<input type="text" class="form-control daterange-cus">
									<div class="input-group-prepend">
										<div class="input-group-text">
											<a id="cetak" href="#" class="btn disabled btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>Kode</th>
										<th>Nama</th>
										<th>Telepon</th>
										<th>Status</th>
										<th>Total Penjualan</th>
										<th>Member</th>
										<th>Tanggal</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var save_method; //for save method string
	var table;

	var date = new Date();
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

	var firstdatestring = firstDay.getFullYear() + "-" + (("0" + (firstDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + firstDay.getDate()).slice(-2);
	var lastdatestring = lastDay.getFullYear() + "-" + (("0" + (lastDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + lastDay.getDate()).slice(-2);

	console.log(firstdatestring);
	console.log(lastdatestring);
	$(document).ready(function() {
		//datatables
		table = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('penjualan/penjualan_all_json'); ?>',
				"type": "POST"
			},
			"order": [
				[6, "desc"]
			],
			//Set column definition initialisation properties.
			"columns": [{
					"data": "kode"
				},
				{
					"data": "nama_penerima"
				},
				{
					"data": "tlp_penerima"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (row.status == 0) {
							return ' <div class="badge badge-info" data-toggle="tooltip" title="Segera Lakukan Follup">Belum Dibayar</div>';
						} else if (row.status == 1) {
							return ' <div class="badge badge-warning" data-toggle="tooltip" title="Segera Lakukan Verifikasi">Minta Verifikasi</div>';
						} else if (row.status == 2) {
							return ' <div class="badge badge-primary" data-toggle="tooltip" title="Paket Sedang Di Kirim">Di Kirim</div>';
						} else if (row.status == 3) {
							return ' <div class="badge badge-success" data-toggle="tooltip" title="Pengiriman Sukses">Selesai</div>';
						} else {
							return ' <div class="badge badge-danger" data-toggle="tooltip" title="Pembayaran DI Tolak">Di Tolak</div>';
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-primary">Rp.' + row.total_penjualan.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</div>';
					}
				},
				{
					"data": "member"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						var today = new Date(row.created_at);
						var dd = String(today.getDate()).padStart(2, '0');
						var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
						var yyyy = today.getFullYear();
						return  dd + '-' + mm + '-' + yyyy;
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("master/penjualan/edit/") ?>' + row.kode + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a>';
					}
				}
			],

		});

		var getFirstDate = firstdatestring;
		var getLastDate = lastdatestring;

		$("#cetak").click(function() {
			window.open("<?php echo site_url('penjualan/print_penjualan/?'); ?>start=" + getFirstDate + "&end=" + getLastDate + "&status=" + $("#select_status").val());
			// console.log(getFirstDate);
			// console.log(getLastDate);
		});

		$('.daterange-cus').daterangepicker({

				locale: {
					format: 'YYYY-MM-DD'
				},
				drops: 'down',
				opens: 'right',
			},
			function(start, end) {
				console.log("Callback has been called!" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));

				table.ajax.url("<?php echo site_url('penjualan/penjualan_all_json/?'); ?>start=" + start.format('YYYY-MM-DD') + "&end=" + end.format('YYYY-MM-DD') + "&status=" + $("#select_status").val()).load();

				getFirstDate = start.format('YYYY-MM-DD');
				getLastDate = end.format('YYYY-MM-DD');
				$("#cetak").removeClass('disabled');

			}).val(firstdatestring + "-" + lastdatestring);


		// Daterangepicker
		if (jQuery().daterangepicker) {
			if ($(".datepicker").length) {
				$('.datepicker').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD'
					},
					singleDatePicker: true,
				});
			}
			if ($(".datetimepicker").length) {
				$('.datetimepicker').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD hh:mm'
					},
					singleDatePicker: true,
					timePicker: true,
					timePicker24Hour: true,
				});
			}
			if ($(".daterange").length) {
				$('.daterange').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD'
					},
					drops: 'down',
					opens: 'right'
				});
			}
		}

	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("barang/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}




	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>