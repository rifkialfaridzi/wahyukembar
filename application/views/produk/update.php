<section class="section">
	<div class="section-header">
		<h1>Halaman Produk</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Edit Produk</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form enctype="multipart/form-data" method="POST" action="<?php echo base_url("barang/update_action/") . $id; ?>" class="needs-validation" novalidate="">

								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
									<div class="col-sm-12 col-md-7">
										<div id="image-preview" class="image-preview">
											<label for="image-upload" id="image-label">Pilih Foto</label>
											<input type="file" name="image" id="image-upload" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Pilih Kategori</label>
									<select name="category" class="form-control select2">
										<option value="">Pilih Kategori</option>
										<?php foreach ($kategori as $kategories) { ?>
											<option <?php $selected = $kategories->id == $category ? "selected" : "";
													echo $selected; ?> value=<?php echo $kategories->id; ?>><?php echo $kategories->nama; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label>Pilih Supplier</label>
									<select name="supplier" class="form-control select2">
										<option value="">Pilih Supplier</option>
										<?php foreach ($supplier as $suppliers) { ?>
											<option <?php $selected = $suppliers->id == $supplier_id ? "selected" : "";
													echo $selected; ?> value=<?php echo $suppliers->id; ?>><?php echo $suppliers->nama; ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label for="nama">Kode Produk</label>
									<input id="nama" type="text" class="form-control" name="kode" value="<?php echo $kode; ?>" tabindex="1" readonly autofocus>
									<div class="invalid-feedback">
										Kode Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Nama Produk</label>
									<input id="nama" type="nama" class="form-control" value="<?php echo $nama; ?>" name="nama" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama Produk Masih Kosong
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label>Pilih Warna</label>
										<select name="warna" class="form-control select2">
											<option value="">Pilih Warna</option>
											<?php foreach ($warna as $warnas) { ?>
												<option <?php $selected = $warnas->id == $warna_id ? "selected" : "";
														echo $selected; ?> value=<?php echo $warnas->id; ?>><?php echo $warnas->nama; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label>Pilih Bahan</label>
										<select name="bahan" class="form-control select2">
											<option value="">Pilih Bahan</option>
											<?php foreach ($bahan as $bahans) { ?>
												<option <?php $selected = $bahans->id == $bahan_id ? "selected" : "";
														echo $selected; ?> value=<?php echo $bahans->id; ?>><?php echo $bahans->nama; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label>Pilih Ukuran</label>
									<select name="ukuran" class="form-control select2">
										<option value="">Pilih Ukuran</option>
										<option <?php $selected = "All Size" == $ukuran ? "selected" : " ";
												echo $selected; ?>  value="All Size">All Size</option>
										<option <?php $selected = "S" == $ukuran ? "selected" : " ";
												echo $selected; ?>  value="S">S</option>
										<option <?php $selected = "M" == $ukuran ? "selected" : " ";
												echo $selected; ?> value="M">M</option>
										<option <?php $selected ="L" == $ukuran ? "selected" : " ";
												echo $selected; ?> value="L">L</option>
										<option <?php $selected ="XL" == $ukuran ? "selected" : " ";
												echo $selected; ?> value="XL">XL</option>
										<option <?php $selected ="XXL" == $ukuran ? "selected" : " ";
												echo $selected; ?> value="XXL">XXL</option>
										<option <?php $selected ="XXXL" == $ukuran ? "selected" : " ";
												echo $selected; ?> value="XXXL">XXXL</option>
									</select>
								</div>
								<div class="form-group">
									<label for="nama">Deskripsi <?php var_dump( $ukuran); ?></label>
									<textarea id="note" type="text" class="form-control" name="note" tabindex="1" required autofocus><?php echo $note; ?></textarea>
									<div class="invalid-feedback">
										Deskripsi Masih Kosong
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="nama">Minimal Stock</label>
										<input id="nama" type="number" value="<?php echo $min_stock; ?>" class="form-control" name="min_stock" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Nama Minimal Stock Masih Kosong
										</div>
									</div>
									<div class="form-group col-md-6">
										<label for="nama">Stock</label>
										<input id="stock" type="number" class="form-control" value="<?php echo $stock; ?>" name="stock" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Stock Masih Kosong
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-8">
										<label for="nama">Harga</label>
										<input id="stock" type="number" value="<?php echo $harga_penjualan; ?>" class="form-control" name="harga_penjualan" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Harga Jual Masih Kosong
										</div>
									</div>
									<div class="form-group col-md-4">
										<label for="nama">Berat</label>
										<div class="input-group">
											<input type="number" value="<?php echo $berat; ?>" step="0.01" class="form-control" name="berat" tabindex="1" required autofocus>
											<div class="input-group-prepend">
												<div class="input-group-text">
													Kg
												</div>
											</div>
											<div class="invalid-feedback">
												Berat Masih Kosong
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Ubah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Unit</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>
											Gambar
										</th>
										<th>Kode</th>
										<th>Nama</th>
										<th>Kategori</th>
										<th>Warna</th>
										<th>Bahan</th>
										<th>Ukuran</th>
										<th>Stock</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('barang/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return ' <img alt="image" src="<?php echo site_url('assets/uploads/'); ?>' + row.image + '" class="rounded-circle" width="35" data-toggle="tooltip" title="Wildan Ahdian">';
					}
				},
				{
					"data": "kode"
				},
				{
					"data": "nama"
				},
				{
					"data": "category"
				},
				{
					"data": "warna"
				},
				{
					"data": "bahan"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-primary" data-toggle="tooltip" title="Stock Yang Tersedia">' + row.ukuran + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (parseInt(row.stock) > parseInt(row.min_stock)) {
							return ' <div class="badge badge-info" data-toggle="tooltip" title="Stock Yang Tersedia">' + row.stock + '</div>';
						} else {
							return ' <div class="badge badge-danger" data-toggle="tooltip" title="Stock Yang Tersedia">' + row.stock + '</div>';
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("master/produk/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("barang/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});

	$("#image-preview").attr("style", "background-image:url('<?php echo site_url("assets/uploads/") . $image; ?>'); background-size:cover; background-position:center center");
</script>