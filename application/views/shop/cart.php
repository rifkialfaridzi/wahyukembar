<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">

    <header class="section-header">
        <?php if ($this->session->flashdata('pesan')) { ?>
            <div class="alert alert-info text-center alert-dismissible show">
                <?php echo $this->session->flashdata('pesan'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>


        <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
            <div class="container">
                <ul class="navbar-nav d-none d-md-flex mr-auto">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/tentang-kami'); ?>">Tentang Kami</a></li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
                </ul> <!-- list-inline //  -->
            </div> <!-- navbar-collapse .// -->
            </div> <!-- container //  -->
        </nav> <!-- header-top-light.// -->

        <section class="header-main border-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-6">
                        <a href="<?php echo base_url('/'); ?>" class="brand-wrap">
                            <img class="logo" src="<?php echo base_url('assets/img/unmouth-rect.png'); ?>">
                        </a> <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-12 col-sm-12">
                        <form action="#" class="search">
                            <div class="input-group w-100">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form> <!-- search-wrap .end// -->
                    </div> <!-- col.// -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="widgets-wrap float-md-right">
                            <?php
                            $data_session = $this->session->userdata;
                            if (isset($data_session['level']) && $data_session['level'] == 2) {
                            ?>
                                <div class="widget-header mr-3">
                                    <a title="Scan Barcode" href="#" id="qrmodal" class="icon icon-sm rounded-circle border"><i class="fas fa-qrcode"></i></a>
                                </div>
                            <?php } ?>
                            <div class="widget-header  mr-3">
                                <a href="#" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i></a>
                                <span class="badge badge-pill badge-danger notify">{{count_carts}}</span>
                            </div>
                            <?php
                            $data_session = $this->session->userdata;
                            if (isset($data_session['username'])) {
                            ?>
                                <div class="widget-header icontext">
                                    <?php if ($data_session['level'] == 1 || $data_session['level'] == 2) { ?>
                                        <a href="<?php echo base_url('admin') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url('shop/member/overview') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                    <?php } ?>
                                    <div class="text">
                                        <span class="text-muted">Selamat Datang!</span>
                                        <div>
                                            <b><?php echo $data_session['username']; ?></b>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="widget-header icontext">
                                    <a href="#" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                    <div class="text">
                                        <span class="text-muted">Selamat Datang!</span>
                                        <div>
                                            <a href="<?php echo base_url('shop/masuk'); ?>">Masuk</a> |
                                            <a href="<?php echo base_url('shop/daftar'); ?>"> Daftar</a>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div> <!-- widgets-wrap.// -->
                    </div> <!-- col.// -->
                </div> <!-- row.// -->
            </div> <!-- container.// -->
        </section> <!-- header-main .// -->
    </header> <!-- section-header.// -->



    <nav class="navbar navbar-main navbar-expand-lg navbar-light border-bottom">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('shop/category/all') ?>"><strong> <i class="fa fa-bars"></i> &nbsp All category</strong></a>
                    </li>
                    <li ng-repeat="category_produks in category_produk" class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('shop/category/') ?>{{ category_produks.id }}">{{ category_produks.nama }}</a>
                    </li>
                </ul>
            </div> <!-- collapse .// -->
        </div> <!-- container .// -->
    </nav>

    </header> <!-- section-header.// -->



    <!-- ========================= SECTION INTRO ========================= -->

    <!-- ========================= SECTION INTRO END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">
        <div class="container">

            <div class="row">

                <output>
                    <div class="row">
                        <aside class="col-lg-8">
                            <div class="card">

                                <div class="table-responsive">

                                    <table class="table table-borderless table-shopping-cart">
                                        <thead class="text-muted">
                                            <tr class="small text-uppercase">
                                                <th scope="col">Product</th>
                                                <th scope="col" width="120">Quantity</th>
                                                <th scope="col" width="120">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data_cart as $key) {
                                                # code...
                                            ?>
                                                <tr>
                                                    <td>
                                                        <figure class="itemside align-items-center">
                                                            <div class="aside"><img src="<?php echo base_url("assets/uploads/") . $key['image']; ?>" class="img-sm"></div>
                                                            <figcaption class="info">
                                                                <a href="#" class="title text-dark"><?php echo $key['name']; ?></a>
                                                                <p class="small text-muted">Kode : <?php echo $key['kode']; ?> <br> Kategori : <?php echo $key['category_name']; ?><br> Stock : <?php echo $key['stock']; ?></p>
                                                            </figcaption>
                                                        </figure>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">

                                                            <input id="update-qty-<?php echo $key['id_product']; ?>" max="<?php $stock_ready = $key['stock'];
                                                                                                                            echo $stock_ready; ?>" value="<?php echo intval($key['qty']) >= intval($stock_ready) ? $stock_ready : $key['qty']; ?>" min="1" type="number" placeholder="Qty" class="form-control qty" name="qty" onKeyUp="if(this.value><?php echo $key['stock'] ?>){this.value='<?php echo $key['stock'] ?>';}else if(this.value<0){this.value='0';}">

                                                        </div>

                                                        <!-- <div class="form-group">
                                                            
                                                            <input id="update-qty-<?php //echo $key['id_product']; 
                                                                                    ?>" max="<?php //$stock_ready = $key['stock']; echo $stock_ready;
                                                                                                                            ?>" value="<?php //echo intval($key['qty']) >= intval($stock_ready) ? $stock_ready : $key['qty']; 
                                                                                                                                                                                                ?>" min="1" type="number" placeholder="Qty" class="form-control qty" name="qty" onKeyUp="if(this.value><?php //echo $key['stock'] - $key['min_stock'] 
                                                                                                                                                                                                                                                                                                                                                                                                ?>){this.value='<?php //echo $key['stock'] - $key['min_stock'] 
                                                                                                                                                                                                                                                                                                                                                                                                                                                            ?>';}else if(this.value<0){this.value='0';}">
                                                           
                                                        </div> -->
                                                    </td>
                                                    <td>
                                                        <div class="price-wrap">
                                                            <var class="price">Rp <?php echo number_format($key['subtotal'], 2, ',', '.'); ?></var>
                                                            <small class="text-muted">Rp <?php echo number_format($key['price'], 2, ',', '.'); ?></small>
                                                        </div> <!-- price-wrap .// -->
                                                    </td>
                                                    <td class="text-right d-none d-md-block">
                                                        <a href="#" class="btn btn-primary btn-round ubah" data-rowid="<?php echo $key['rowid']; ?>" data-productid="<?php echo $key['id_product']; ?>"> Ubah</a>
                                                        <a href="#" class="btn btn-danger btn-round hapus" data-rowid="<?php echo $key['rowid']; ?>" data-productid="<?php echo $key['id_product']; ?>"> Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>

                                </div> <!-- table-responsive.// -->

                                <div class="card-body border-top">
                                    <p class="icontext"><i class="icon text-success fa fa-truck"></i>Ongkir</p>
                                </div> <!-- card-body.// -->

                            </div> <!-- card.// -->

                        </aside> <!-- col.// -->
                        <aside class="col-lg-4">

                            <div class="card mb-3">
                                <div class="card-body">
                                    <dl class="dlist-align">
                                        <dt>Total:</dt>
                                        <dd class="text-right text-dark b"><strong>Rp <?php echo number_format($total_cart, 2, ',', '.'); ?></strong></dd>
                                    </dl>
                                    <dl class="dlist-align">
                                        <dt>Diskon:</dt>
                                        <dd class="text-right text-dark b"><strong>Rp <?php
                                                                                        if (isset($this->session->userdata['level'])) {
                                                                                            if ($this->session->userdata['level'] == 2) {
                                                                                                echo number_format(0, 2, ',', '.');
                                                                                            } else {

                                                                                                $diskon = ($total_cart / 100) * $data_diskon->jumlah;

                                                                                                echo number_format($diskon, 2, ',', '.');
                                                                                            }
                                                                                        }


                                                                                        ?></strong></dd>
                                    </dl>




                                </div> <!-- card-body.// -->
                            </div> <!-- card.// -->

                            <div class="card mb-3">
                                <div class="card-body">
                                    <article class="card-body">
                                        <header class="mb-4">
                                            <h4 class="card-title">Data Pemesanan</h4>
                                        </header>
                                        <form method="POST" action="<?php echo base_url("shop/checkout"); ?>" class="needs-validation" novalidate="">
                                            <div class="form-group">
                                                <label>Nama Penerima</label>
                                                <input type="text" name="nama_penerima" class="form-control" placeholder="" value="<?php if (isset($data_detail_user->nama_penerima)) {
                                                                                                                                        echo $data_detail_user->nama_penerima;
                                                                                                                                    } ?>" tabindex="1" required autofocus>
                                                <small class="form-text text-muted">Nama Penerima Pesanan</small>
                                                <div class="invalid-feedback">
                                                    Nama Penerima Masih Kosong
                                                </div>
                                            </div> <!-- form-group end.// -->
                                            <?php if ($this->session->userdata('level') != 2) { ?>
                                                <div class="form-group">
                                                    <label>Kota Pengiriman</label>
                                                    <select id="inputState" name="kurir" class="form-control" tabindex="1" required autofocus>
                                                        <?php foreach ($data_kuir as $key) {
                                                        ?>
                                                            <option value="<?php echo $key->id;  ?>"><?php echo $key->alamat . " - " . "Rp" . number_format($key->harga, 2, ',', '.');  ?></option>
                                                        <?php
                                                        } ?>
                                                    </select>
                                                    <small class="form-text text-muted">Harga ongkos kirim tiap kota</small>
                                                    <div class="invalid-feedback">
                                                        Kota Pengiriman Masih Kosong
                                                    </div>
                                                </div> <!-- form-group end.// -->
                                                <div class="form-group">
                                                    <label>Alamat Detail</label>
                                                    <textarea name="alamat_penerima" class="form-control" tabindex="1" required autofocus><?php if (isset($data_detail_user->alamat_penerima)) {
                                                                                                                                                echo $data_detail_user->alamat_penerima;
                                                                                                                                            } ?></textarea>
                                                    <small class="form-text text-muted">Alamat detail pengiriman</small>
                                                    <div class="invalid-feedback">
                                                        Alamat detail Masih Kosong
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nomor WA</label>
                                                    <input type="number" class="form-control" value="<?php if (isset($data_detail_user->tlp)) {
                                                                                                            echo $data_detail_user->tlp;
                                                                                                        } ?>" placeholder="" name="tlp" tabindex="1" required autofocus>
                                                    <small class="form-text text-muted">Nomor Whatsapp yang aktif</small>
                                                    <div class="invalid-feedback">
                                                        Nomor WA Masih Kosong
                                                    </div>
                                                </div> <!-- form-group end.// -->
                                            <?php } ?>
                                            <div class="form-group mt-3">
                                                <button type="submit" class="btn btn-primary btn-block"> Checkout </button>
                                                <a href="<?php echo base_url("/"); ?>" class="btn btn-light btn-block">Lanjutkan Belanja</a>
                                            </div> <!-- form-group// -->
                                        </form>
                                    </article>
                                </div> <!-- card-body.// -->
                            </div> <!-- card.// -->



                        </aside> <!-- col.// -->
                    </div> <!-- row.// -->
                </output>
            </div>

        </div>
        <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <div class="modal" id="coba" tabindex="-1" role="dialog">
        <div id="apps" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scan Barcode</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="preview-container"><video id="preview" autoplay="autoplay" class="active" style="transform: scaleX(-1);"></video></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>




    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        var app = new Vue({
            el: '#apps',
            data: {
                scanner: null,
                activeCameraId: null,
                cameras: [],
                scans: []
            },
            mounted: function() {
                var self = this;
                self.scanner = new Instascan.Scanner({
                    video: document.getElementById('preview'),
                    scanPeriod: 5
                });
                self.scanner.addListener('scan', function(content, image) {
                    self.scans.unshift({
                        date: +(Date.now()),
                        content: content
                    });

                    // DATA RESULT

                    var xhr = new XMLHttpRequest();
                    var url = "<?php echo base_url("cart/scan_product"); ?>";

                    var data = JSON.stringify({
                        kode_product: content,
                    });

                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    xhr.onload = function() {
                        location.reload();
                    };

                    xhr.send(data);
                    return false;
                });
                Instascan.Camera.getCameras().then(function(cameras) {
                    self.cameras = cameras;
                    if (cameras.length > 0) {
                        self.activeCameraId = cameras[0].id;
                        self.scanner.start(cameras[0]);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function(e) {
                    console.error(e);
                });
            },
            methods: {
                formatName: function(name) {
                    return name || '(unknown)';
                },
                selectCamera: function(camera) {
                    this.activeCameraId = camera.id;
                    this.scanner.start(camera);
                }
            }
        });

        $("#qrmodal").click(function() {
            $('#coba').modal('toggle');
        });
        $(".ubah").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = $('#update-qty-' + productid).val();

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: qty,
                productid: productid
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        $(".hapus").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = 0;

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: 0,
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("shop/landing_page_data"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.top_produk = response.data.data_top_produk;
                    $scope.new_produk = response.data.data_new_produk;
                    $scope.category_produk = response.data.data_category;

                    // $scope.total_items = response.data.totalItems;
                    // $scope.items = response.data.items;

                    // $scope.baju = $scope.items;
                    // $scope.currentPage = 1;
                    // $scope.itemsPerPage = 20;
                    // $scope.maxSize = 20;
                    // $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            var allProduct = function() {
                $http.get('<?php echo base_url("shop/product_allcategory"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.all_produk = response.data;

                    // $scope.total_items = response.data.totalItems;
                    // $scope.items = response.data.items;

                    // $scope.baju = $scope.items;
                    // $scope.currentPage = 1;
                    // $scope.itemsPerPage = 20;
                    // $scope.maxSize = 20;
                    // $scope.totalItems = $scope.total_items;

                    console.log($scope.all_produk);
                }, function(response) {
                    console.log('error bos');
                });
            }



            $scope.funct_kategori = function(id) {
                $http.get('<?php echo base_url("shop/product_bycategory/"); ?>' + id, {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.all_produk = response.data;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_cart = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();
            allProduct();


            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>