<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets\teleo.png') ?>" />
    <title>Chat Dengan Admin</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- CSS Libraries -->
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap-daterangepicker/daterangepicker.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/datatables.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/select2/dist/css/select2.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/jquery-selectric/selectric.css"); ?>>


    <!-- Template CSS -->
    <link rel="stylesheet" href=<?php echo base_url("assets/css/style.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/css/components.css"); ?>>
    <!-- Start GA -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script src=<?php echo base_url("assets/modules/jquery.min.js"); ?>></script>

    <!-- JS Libraies -->
    <script src=<?php echo base_url("assets/modules/datatables/datatables.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/jquery-ui/jquery-ui.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js"); ?>></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>


    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>
    <!-- /END GA -->
</head>

<body>

    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="card chat-box card-success" id="mychatbox2">
                            <div class="card-header">
                                <h4><i class="fas fa-circle text-success mr-2" title="Online" data-toggle="tooltip"></i> Chat dengan admin</h4>
                            </div>
                            <div class="card-body chat-content">
                                <?php foreach ($data_chat as $data_chats) { ?>

                                    <?php if ($data_chats->send_by != 1) { ?>
                                        <div class="chat-item chat-right" style=""><img src="<?php echo base_url("/assets/img/avatar/avatar-2.png"); ?>">
                                            <div class="chat-details">
                                                <div class="chat-text"><?php echo $data_chats->content; ?></div>
                                                <div class="chat-time"><?php echo $data_chats->tanggal; ?></div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="chat-item chat-left" style=""><img src="<?php echo base_url("/assets/img/avatar/avatar-5.png"); ?>">
                                            <div class="chat-details">
                                                <div class="chat-text"><?php echo $data_chats->content; ?></div>
                                                <div class="chat-time"><?php echo $data_chats->tanggal; ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="card-footer chat-form">
                                <form method="POST" action="<?php echo base_url("shop/kirim_chat/");?>" class="needs-validation" novalidate="" id="chat-form2">
                                    <input type="text" name="content" class="form-control" placeholder="Type a message">
                                    <button class="btn btn-primary">
                                        <i class="far fa-paper-plane"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="card-footer">
                           
                           <!-- <input class="form-control" type="file" id="formFileMultiple" multiple /> -->
                        </div>

                        </div>
                        
                        <div class="simple-footer">
                            Copyright &copy; Wahyu Kembar 2022
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->

    <script src=<?php echo base_url("assets/modules/popper.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/tooltip.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/bootstrap/js/bootstrap.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/nicescroll/jquery.nicescroll.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/moment.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/js/stisla.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/bootstrap-daterangepicker/daterangepicker.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/select2/dist/js/select2.full.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/jquery-selectric/jquery.selectric.min.js"); ?>></script>




    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src=<?php echo base_url("assets/js/scripts.js"); ?>></script>
    <script src=<?php echo base_url("assets/js/custom.js"); ?>></script>
</body>