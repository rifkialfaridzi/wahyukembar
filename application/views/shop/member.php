<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/datatables.min.css"); ?>>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css"); ?>>


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <script src=<?php echo base_url("assets/modules/datatables/datatables.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"); ?>></script>
    <script src=<?php echo base_url("assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"); ?>></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <!-- custom javascript -->
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">
    <?php if ($this->session->flashdata('pesan')) { ?>
        <div class="alert alert-danger text-center alert-dismissible show">
            <?php echo $this->session->flashdata('pesan'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
        <div class="container">
            <ul class="navbar-nav d-none d-md-flex mr-auto">
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                <li class="nav-item <?php if ($this->uri->segment(2) == "pengiriman") {
                                        echo 'active';
                                    } ?>"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
            </ul> <!-- list-inline //  -->
        </div> <!-- navbar-collapse .// -->
        </div> <!-- container //  -->
    </nav> <!-- header-top-light.// -->
    <section class="header-main border-bottom">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 col-6">
                    <a href="<?php echo base_url('/'); ?>" class="brand-wrap">
                        <img class="logo" src="<?php echo base_url('assets/img/unmouth-rect.png'); ?>">
                    </a> <!-- brand-wrap.// -->
                </div>
                <div class="col-lg-6 col-12 col-sm-12">
                    <form action="#" class="search">
                        <div class="input-group w-100">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form> <!-- search-wrap .end// -->
                </div> <!-- col.// -->
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="widgets-wrap float-md-right">
                        <div class="widget-header  mr-3">
                            <a href="<?php echo base_url('shop/cart') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i></a>
                            <span class="badge badge-pill badge-danger notify">{{count_carts}}</span>
                        </div>
                        <?php

                        $data_session = $this->session->userdata;
                        if (isset($data_session['username'])) {
                        ?>
                            <div class="widget-header icontext">
                                <a href="<?php echo base_url('shop/member/overview') ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                <div class="text">
                                    <span class="text-muted">Selamat Datang!</span>
                                    <div>
                                        <b><?php echo $data_session['username']; ?></b>
                                    </div>
                                </div>
                            </div>
                        <?php
                        } else {
                        ?>
                            <div class="widget-header icontext">
                                <a href="#" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                <div class="text">
                                    <span class="text-muted">Selamat Datang!</span>
                                    <div>
                                        <a href="<?php echo base_url('shop/masuk'); ?>">Masuk</a> |
                                        <a href="<?php echo base_url('shop/daftar'); ?>"> Daftar</a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div> <!-- widgets-wrap.// -->
                </div> <!-- col.// -->
            </div> <!-- row.// -->
        </div> <!-- container.// -->
    </section> <!-- header-main .// -->
    </header> <!-- section-header.// -->


    <!-- ========================= SECTION CONTENT ========================= -->

    <!-- ========================= SECTION PAGETOP ========================= -->
    <section class="section-pagetop bg">
        <div class="container">
            <h2 class="title-page">My account</h2>
        </div> <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">
        <div class="container">

            <div class="row">
                <aside class="col-md-3">
                    <ul class="list-group">
                        <a class="list-group-item <?php if ($this->uri->segment(3) == "overview") {
                                                        echo 'active';
                                                    } ?>" href="<?php echo base_url('shop/member/overview'); ?>"> Account overview </a>
                        <a class="list-group-item <?php if ($this->uri->segment(3) == "order-list") {
                                                        echo 'active';
                                                    } ?>" href="<?php echo base_url('shop/member/order-list'); ?>"> My orders </a>
                        <a class="list-group-item <?php if ($this->uri->segment(3) == "order-received") {
                                                        echo 'active';
                                                    } ?>" href="<?php echo base_url('shop/member/order-received'); ?>"> History orders </a>
                        <a class="list-group-item <?php if ($this->uri->segment(3) == "setting") {
                                                        echo 'active';
                                                    } ?> " href="<?php echo base_url('shop/member/setting'); ?>">Setting</a>
                    </ul>
                    <br>
                    <a onclick="openWin()" class="btn btn-success btn-block" href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> <span class="text">Chat Customer Service</span> </a>
                    <a class="btn btn-light btn-block" href="<?php echo base_url('auth/logout'); ?>"> <i class="fa fa-power-off"></i> <span class="text">Log out</span> </a>
                </aside> <!-- col.// -->
                <?php $this->load->view($main_content); ?>
            </div>

        </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->


    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        function openWin() {
            myWindow = window.open("<?php echo base_url('shop/chat'); ?>", "", "width=380, height=520");
        }


        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }
            $scope.count_cart();
        });

        $(document).ready(function() {



            // jQuery code
            table_list = $('#order_list_tabel').DataTable({
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": '<?php echo site_url('shop/transaksi_json'); ?>',
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columns": [{
                        "data": "kode"
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            return "Rp." + parseInt(row.total_penjualan).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            if (row.status == 0) {
                                return ' <div class="badge badge-warning" tittle="Segera Lakukan Pembayaran">Belum Di Bayar</div>';
                            } else if (row.status == 1) {
                                return ' <div class="badge badge-primary" tittle="Menunggu Verifikasi Pembayaran">Sudah Dibayar(Menunggu Validasi)</div>';
                            } else if (row.status == 2) {
                                return ' <div class="badge badge-info" tittle="Pesanan Sudah DI Proses">Sedang Di Kirim</div>';
                            } else if (row.status == 3) {
                                return ' <div class="badge badge-success" tittle="Pesanan Sudah Sampai">Selesai</div>';
                            } else {
                                return ' <div class="badge badge-danger" tittle="Bukti Pembayaran Tidak Valid">Pembayaran Ditolak</div>';
                            }

                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            var today = new Date(row.created_at);
                            var dd = String(today.getDate()).padStart(2, '0');
                            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                            var yyyy = today.getFullYear();
                            return dd + '-' + mm + '-' + yyyy;


                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            return '<a href="<?php echo site_url("shop/invoice/") ?>' + row.kode + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle"></i></a>';

                        }
                    }
                ],

            });

            table_history = $('#order_history_tabel').DataTable({
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": '<?php echo site_url('shop/riwayat_transaksi_json'); ?>',
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columns": [{
                        "data": "kode"
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            return "Rp." + parseInt(row.total_penjualan).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            return ' <div class="badge badge-success">Selesai</div>';

                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            var today = new Date(row.created_at);
                            var dd = String(today.getDate()).padStart(2, '0');
                            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                            var yyyy = today.getFullYear();
                            return dd + '-' + mm + '-' + yyyy;


                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row) {
                            return '<a href="<?php echo site_url("shop/invoice/") ?>' + row.kode + '" class="btn btn-icon btn-primary"><i class="fas fa-info-circle"></i></a>';
                        }
                    }
                ],

            });
        });
    </script>
</body>

</html>