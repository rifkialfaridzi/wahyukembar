<main class="col-md-9">


	<article class="card  mb-3">
		<div class="card-body">
			<h5 class="card-title mb-4">History Transaksi</h5>

			<div class="row">
				<div class="table-responsive">
					<table id="order_history_tabel" class="table table-striped">
						<thead>
							<tr>
								<th>
									#Kode
								</th>
								<th>Total</th>
								<th>Status</th>
								<th>Tanggal</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div> <!-- row.// -->
		</div> <!-- card-body .// -->
	</article> <!-- card.// -->

</main> <!-- col.// -->