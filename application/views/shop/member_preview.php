<main class="col-md-9">

	<article class="card mb-3">
		<div class="card-body">

			<figure class="icontext">
				<div class="icon">
					<img class="rounded-circle img-sm border" src="<?php echo base_url('assets/img/avatar/avatar-1.png') ?>">
				</div>
				<div class="text">
					<strong> <?php echo $data_member->name; ?> </strong> <br>
					<?php echo $data_member->email; ?><br>
				</div>
			</figure>
			<hr>
			<p>
				<i class="fa fa-map-marker text-muted"></i> &nbsp; My address:
				<br>
				<?php echo $data_member->alamat_penerima; ?>
			</p>

			<article class="card-group">
				<figure class="card bg">
					<div class="p-3">
						<h5 class="card-title"><?php echo $data_order['orders']; ?></h5>
						<span>Orders</span>
					</div>
				</figure>
				<figure class="card bg">
					<div class="p-3">
						<h5 class="card-title"><?php echo $data_order['processed_orders']; ?></h5>
						<span>Awaiting delivery</span>
					</div>
				</figure>
				<figure class="card bg">
					<div class="p-3">
						<h5 class="card-title"><?php echo $data_order['delivered_orders']; ?></h5>
						<span>Delivered items</span>
					</div>
				</figure>
			</article>


		</div> <!-- card-body .// -->
	</article> <!-- card.// -->

	<article class="card  mb-3">
		<div class="card-body">
			<h5 class="card-title mb-4">Recent orders</h5>

			<div class="row">
				<?php foreach ($recent_order as $key) { ?>
					<div class="col-md-6">
						<figure class="itemside mb-3">
							<div class="aside"><img src="<?php echo base_url('assets/uploads/').$key->image; ?>" class="border img-sm"></div>
							<figcaption class="info">
								<time class="text-muted"><i class="fa fa-calendar-alt"></i> <?php echo date('d-m-Y', strtotime($key->created_at)); ?></time>
								<p><?php echo $key->nama_barang; ?></p>
								<p><small><?php echo $key->kode; ?></small></p>

								<?php switch ($key->status) {
									case '0':
										echo "<span class='text-warning'>Belum Dibayar</span>";
										break;
									case '1':
										echo "<span class='text-primary'>Sudah Dibayar(Menunggu Validasi)</span>";
										break;
									case '2':
										echo "<span class='text-info'>Sedang Dikirim</span>";
										break;
									case '3':
										echo "<span class='text-success'>Sudah Diterima</span>";
										break;

									default:
										echo "<span class='text-success'>Selesai</span>";
										break;
								} ?>
							</figcaption>
						</figure>
					</div> <!-- col.// -->
				<?php } ?>
			</div> <!-- row.// -->
		</div> <!-- card-body .// -->
	</article> <!-- card.// -->

</main> <!-- col.// -->