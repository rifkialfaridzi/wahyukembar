<main class="col-md-9">

    <article class="card  mb-3">
        <div class="card-body">
            <h3 class="card-title mb-4">Member Setting </h3>

            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="<?php echo base_url("shop/setting_action"); ?>" class="needs-validation" novalidate="">

                        <div class="form-group">
                            <label>Nomor WA</label>
                            <input type="number" name="tlp" value="<?php echo $data_member->tlp; ?>" class="form-control" placeholder="" tabindex="1" required autofocus>
                            <div class="invalid-feedback">
                                Nomor WA Category Masih Kosong
                            </div>
                        </div> <!-- form-group end.// -->

                        <div class="form-group">
                            <label>Nama Penerima</label>
                            <input id="nama_penerima" value="<?php echo $data_member->nama_penerima; ?>" type="text" name="nama_penerima" class="form-control" placeholder="" tabindex="1" required autofocus>
                            <div class="invalid-feedback">
                                Alamat Penerima Masih Kosong
                            </div>
                        </div> <!-- form-group end.// -->

                        <div class="form-group">
                            <label>Kota Pengiriman</label>
                            <select id="inputState" name="kurir" class="form-control" tabindex="1" required autofocus>
                                <option value="">pilih</option>

                                <?php foreach ($data_kuir as $key) {
                                ?>
                                    <option <?php $select = $key->id == $data_member->kurir ? "selected" : "";
                                            echo $select; ?> value="<?php echo $key->id;  ?>"><?php echo $key->alamat . " - " . "Rp" . number_format($key->harga, 2, ',', '.');  ?></option>
                                <?php
                                } ?>
                            </select>
                            <small class="form-text text-muted">Harga ongkos kirim tiap kota</small>
                            <div class="invalid-feedback">
                                Kota Pengiriman Masih Kosong
                            </div>
                        </div> <!-- form-group end.// -->
                        <div class="form-group">
                            <label>Alamat Penerima</label>
                            <textarea id="alamat_penerima" type="text" name="alamat_penerima" class="form-control" placeholder="" tabindex="1" required autofocus><?php echo $data_member->alamat_penerima; ?></textarea>
                            <div class="invalid-feedback">
                                Alamat Penerima Masih Kosong
                            </div>
                        </div> <!-- form-group end.// -->


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"> Ubah </button>
                        </div> <!-- form-group// -->
                    </form>
                </div> <!-- col.// -->
            </div> <!-- row.// -->
        </div> <!-- card-body .// -->
    </article> <!-- card.// -->

</main> <!-- col.// -->