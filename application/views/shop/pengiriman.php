<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <!-- custom javascript -->
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">
    <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
        <div class="container">
            <ul class="navbar-nav d-none d-md-flex mr-auto">
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                <li class="nav-item <?php if ($this->uri->segment(2) == "pengiriman") {
                echo 'active';
            } ?>"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/tentang-kami'); ?>">Tentang Kami</a></li>
            </ul>
            <ul class="navbar-nav">
            <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
            </ul> <!-- list-inline //  -->
        </div> <!-- navbar-collapse .// -->
        </div> <!-- container //  -->
    </nav> <!-- header-top-light.// -->
    </header> <!-- section-header.// -->


    <!-- ========================= SECTION CONTENT ========================= -->
    
<section class="section-content padding-y">
<div class="container">

<header class="section-heading">
	<h2 class="section-title">Panduan Pengiriman</h2>
</header><!-- sect-heading -->

<article>


<p>Saat ini Wahyu Kembar berusaha untuk melakukan pengiriman ke seluruh pelosok dan kota yang ada di Indonesia, menggunakan jasa ekspedisi JNE, TIKI dan POS Indonesia, dengan menggunakan layanan yang disediakan oleh pihak ekspedisi. Di kemudian hari, tidak menutup kemungkinan bagi Wahyu Kembar untuk menambah ekspedisi lainnya, agar seluruh konsumen kami dapat membeli dan menerima produk mereka di tempatnya di seluruh wilayah Indonesia.</p>
<p>Seluruh pemesanan produk yang kami terima setelah pukul 16.00 WIB melalui situs website kami akan dilakukan di hari berikutnya. Jam pengiriman akan dilakukan sesuai dengan hari kerja.</p>
<p>Estimasi waktu pengiriman tergantung pada metode pengiriman yang Anda pilih selama checkout, semua biaya pengiriamn gratis. Harap dicatat bahwa pesanan Anda hanya akan dikirimkan ketika semua barang pesanan tersedia dan pembayaran pesanan telah selesai dilakukan.</p>

<p>Setelah Anda melakukan pemesanan, Anda akan menerima pesan Whatsapp konfirmasi rincian pesanan Anda. Setelah item Anda dikirimkan, Anda akan menerima pesan Whatsapp Pengiriman Pemberitahuan dari operator dan informasi pelacakan.</p>

<br><br><br><br><br><br><br><br><br>

</article>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->


    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        
    </script>
</body>

</html>