<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
    <style>
        html {
            margin: 10px 0 10px 0
        }

        h3 {
            font-size: 18px;
        }

        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg .tg-lboi {
            border-color: inherit;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div class="container">
        <style type="text/css">
            .tg {
                border-collapse: collapse;
                border-spacing: 0;
            }

            .tg td {
                font-family: Arial, sans-serif;
                font-size: 14px;
                padding: 10px 5px;
                word-break: normal;

            }

            .tg th {
                font-family: Arial, sans-serif;
                font-size: 14px;
                font-weight: normal;
                padding: 10px 5px;
                overflow: hidden;
                word-break: normal;
            }

            .tg .tg-0pky {
                border-color: inherit;
                text-align: left;
                vertical-align: top
            }
        </style>
        <table class="tg" style="table-layout: fixed; width: 100%; border:solid 1px;">
            <colgroup>
                <col style="width: 20%">
                <col style="width: 60%">
                <col style="width: 20%">
            </colgroup>
            <tr>
                <th class="tg-0pky">
                    <img alt="image" style="width:80%; margin:5px; text-align:center" src="<?php echo site_url($image_logo); ?>">
                    <p style="margin:5px 0 5px 0;font-size:12px; vertical-align: middle;">Jl. Veteran, Randulawang, Jetis, Kec. Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57511</p>
                    <p style="margin:5px 0 5px 0;font-size:12px">Tlp.0821-2646-6492</p>
                </th>
                <th class="tg-0pky" style="text-align:center;vertical-align: middle">
                    <h4> Nota Pembelian </h4>
                </th>
                <th class="tg-0pky" style="text-align: center">
                    <img alt="image" style="width:35%; margin:5px" src="">
                    <p style="margin: 2px 0 2px 0;font-size: 8"><?php echo $data_penjualan->kode; ?></p>
                </th>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="3">
                    <table class="tg" style="width:100%;">
                        <tr>
                            <th class="tg-cly1" colspan="3" style="padding:0;">
                                <table style="padding:0;">
                                    <tr>
                                        <th style="padding:3px">
                                           <?php if($this->session->userdata('level') != 2){ ?> <p style="margin:0;">Nama Pembeli</p> <?php } ?>
                                        </th>
                                        <th style="padding:2px">
                                        <?php if($this->session->userdata('level') != 2){ ?>
                                            <p style="margin:0;">: <?php if (isset($data_penjualan_pelanggan->nama)) {
                                                                        echo $data_penjualan_pelanggan->nama;
                                                                    } else {
                                                                        echo $data_penjualan_pelanggan->nama_penerima;
                                                                    } ?></p>
                                        <?php } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:2px">
                                        <?php if($this->session->userdata('level') != 2){ ?><p style="margin:0;">Nomor Tlp</p> <?php } ?>
                                        </th>
                                        <th style="padding:2px">
                                        <?php if($this->session->userdata('level') != 2){ ?><p style="margin:0;">: <?php echo $data_penjualan_pelanggan->tlp; ?></p> <?php } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:0">
                                        <?php if($this->session->userdata('level') != 2){ ?><p style="margin:0;">Alamat</p> <?php } ?>
                                        </th>
                                        <th style="padding:0">
                                        <?php if($this->session->userdata('level') != 2){ ?> <p style="margin:0;">: <?php echo $data_penjualan_pelanggan->alamat_penerima; ?></p> <?php } ?>
                                        </th>
                                    </tr>
                                </table>
                            </th>

                            <th class="tg-cly1" colspan="2" style="padding:0;">
                                <table style="padding:0;">
                                    <tr>
                                        <th style="padding:2px">
                                        <?php if($this->session->userdata('level') != 2){ ?> <p style="margin:0;">Pembayaran</p> <?php } ?>
                                        </th>
                                        <th style="padding:2px">
                                        <?php if($this->session->userdata('level') != 2){ ?>
                                            <?php if ($data_penjualan->status == 0) { ?>
                                                <p style="margin:0;">: Menunggu Pembayaran</p>
                                            <?php } elseif ($data_penjualan->status >= 1 && $data_penjualan->status <= 3) { ?>
                                                <p style="margin:0;">: <b><?php echo $data_penjualan_payment->nama . " - " . $data_penjualan_payment->rekening; ?></b></p>

                                            <?php } elseif ($data_penjualan->status == 4) { ?>
                                                <span class="text-danger">
                                                    <p style="margin:0;">: <b>Pembayaran Ditolak - Data Tidak Valid</b></p>

                                                </span>
                                            <?php } ?>
                                         <?php } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:2px">
                                            <p style="margin:0;">Nama</p>
                                        </th>
                                        <th style="padding:2px">
                                            <p style="margin:0;">: <?php if (isset($data_penjualan_pelanggan->nama)) {
                                                                        echo $data_penjualan_pelanggan->nama;
                                                                    } else {
                                                                        echo $data_penjualan_pelanggan->nama_penerima;
                                                                    } ?></p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:0">
                                            <p style="margin:0;">Status</p>
                                        </th>
                                        <th style="padding:0">
                                            <?php if ($data_penjualan->status == 0) { ?>
                                                <p style="margin:0;">: Menunggu</p>
                                            <?php } elseif ($data_penjualan->status >= 1 && $data_penjualan->status <= 3) { ?>
                                                <p style="margin:0;">: <b>Pesanan Di Terima</b></p>

                                            <?php } elseif ($data_penjualan->status == 4) { ?>
                                                <span class="text-danger">
                                                    <p style="margin:0;">: <b>Pembayaran Ditolak - Data Tidak Valid</b></p>

                                                </span>
                                            <?php } ?>
                                        </th>
                                    </tr>
                                </table>
                            </th>
                        </tr>
                        <tr>
                            <td style="border:solid 1px;padding:2px;width:5%">
                                <p style="font-weight:bold;margin:0">No.</p>
                            </td>
                            <td style="border:solid 1px;padding:2px;width:40%">
                                <p style="font-weight:bold;margin:0">Nama Produk</p>
                            </td>
                            <td style="border:solid 1px;padding:2px;width:5%;text-align:center">
                                <p style="font-weight:bold;margin:0">Qty</p>
                            </td>
                            <td style="border:solid 1px;padding:2px;width:20%">
                                <p style="font-weight:bold;margin:0">Harga</p>
                            </td>
                            <td style="border:solid 1px;padding:2px;width:30%">
                                <p style="font-weight:bold;margin:0">Total</p>
                            </td>
                        </tr>
                        <?php $a = 1;
                        foreach ($data_penjualan_detail as $key) { ?>
                            <tr>
                                <td class="tg-cly1" style="border:solid 1px;padding:2px"><?php echo $a++; ?></td>
                                <td class="tg-cly1" style="border:solid 1px;padding:2px"><?php echo $key->nama; ?></td>
                                <td class="tg-cly1" style="border:solid 1px;padding:2px;text-align:center"><?php echo $key->jumlah; ?></td>
                                <td class="tg-0lax" style="border:solid 1px;padding:2px">Rp. <?php echo number_format($key->harga_penjualan, 2); ?></td>
                                <td class="tg-0lax" style="border:solid 1px;padding:2px">Rp. <?php echo number_format($key->total, 2); ?></td>
                            </tr>
                        <?php } ?>

                        <tr>

                            <td class="tg-0lax" colspan="2">
                                <p style="text-align:right;margin:0">Diskon </p>
                                <p style="text-align:right;margin:0">Ongkir </p>
                                <p style="text-align:right;margin:0">Total Pembayaran </p>
                            </td>
                            <td class="tg-0lax"></td>
                            <td class="tg-0lax"></td>
                            <td class="tg-0lax">
                                <p style="font-weight:bold"></p>



                                <p style="text-align:right;margin:0;font-weight:bold">Rp. <?php $nominal = $data_penjualan_diskon->jumlah == null ? 0 : $data_penjualan_diskon->jumlah;
                                                                                            $diskon = ($data_penjualan_diskon->total_penjualan / 100) * $nominal;
                                                                                            echo number_format($diskon, 2); ?></p>
                                <p style="text-align:right;margin:0;font-weight:bold">Rp. <?php echo number_format($data_penjualan->ongkir, 2); ?></p>
                                <p style="text-align:right;margin:0;font-weight:bold">Rp. <?php $final_total = ($key->total_penjualan - $diskon) + $data_penjualan->ongkir;
                                                                                            echo number_format($final_total, 2); ?></p>
                            </td>
                        </tr>

                        <tr>

                            <td class="tg-0lax" colspan="2">
                                <p style="text-align:left;font-weight: bold">Tanggal : <?php $time = strtotime($data_penjualan->created_at);
                                                                                        echo date('d M Y    ', $time); ?></p>
                            </td>
                            <td class="tg-0lax"></td>
                            <td class="tg-0lax"></td>
                            <td class="tg-0lax">
                                <p style="font-weight:bold"></p>
                            </td>
                        </tr>
                    </table>
                </td>

                </td>
            </tr>
        </table>


    </div>
</body>

</html>