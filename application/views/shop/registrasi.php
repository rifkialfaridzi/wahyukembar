<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Wahyu Kembar</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/modules/jquery.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />



    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body>
    <?php if ($this->session->flashdata('pesan')) { ?>
        <div class="alert alert-info text-center alert-dismissible show">
            <?php echo $this->session->flashdata('pesan'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>

    <header class="section-header">
        <nav class="navbar navbar-dark navbar-expand p-0" style="background-color:#3167eb">
            <div class="container">
                <ul class="navbar-nav d-none d-md-flex mr-auto">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop'); ?>">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pengiriman'); ?>">Pengiriman</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/pembayaran'); ?>">Pembayaran</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('shop/tentang-kami'); ?>">Tentang Kami</a></li>
                </ul>
                <ul class="navbar-nav">
                <li class="nav-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=6282126466492&text=Halo%20kak,%20Saya%20mau%20order...." class="nav-link"> (WA): +6282126466492</a></li>
                </ul> <!-- list-inline //  -->
            </div> <!-- navbar-collapse .// -->
            </div> <!-- container //  -->
        </nav> <!-- header-top-light.// -->
    </header> <!-- section-header.// -->


    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">

        <!-- ============================ COMPONENT REGISTER   ================================= -->
        <div class="card mx-auto" style="max-width:520px; margin-top:40px;">
            <article class="card-body">
                <header class="mb-4">
                    <h4 class="card-title">Sign up</h4>
                </header>
                <form method="POST" action="<?php echo base_url("auth/do_registration/2"); ?>" class="needs-validation" novalidate="" oninput='password_conf.setCustomValidity(password_conf.value != password.value ? "Passwords do not match." : "")'>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" name="name" class="form-control" placeholder="" tabindex="1" required autofocus>
                        <div class="invalid-feedback">
                            Nama Masih Kosong
                        </div>
                    </div> <!-- form-group end.// -->
                    <div class="form-group">
                        <label>Username</label>
                        <input id="username" type="text" name="username" class="form-control" placeholder="" tabindex="1" required autofocus>
                        <div class="invalid-feedback">
                           Username Masih Kosong
                        </div>
                    </div> <!-- form-group end.// -->
                    <div class="form-group">
                        <label>Nomor WA</label>
                        <input type="number" name="tlp" class="form-control" placeholder="" tabindex="1" required autofocus>
                        <div class="invalid-feedback">
                            Nomor WA Category Masih Kosong
                        </div>
                    </div> <!-- form-group end.// -->
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="" tabindex="1" required autofocus>
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                        <div class="invalid-feedback">
                            Email Masih Kosong
                        </div>
                    </div> <!-- form-group end.// -->
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Create password</label>
                            <input class="form-control" name="password" type="password" tabindex="1" required autofocus>
                            <div class="invalid-feedback">
                                Password Masih Kosong
                            </div>
                        </div> <!-- form-group end.// -->
                        <div class="form-group col-md-6">
                            <label>Repeat password</label>
                            <input class="form-control" name="password_conf" type="password" tabindex="1" required autofocus>
                            <div class="invalid-feedback">
                                Masukkan Ulang Password
                            </div>
                        </div> <!-- form-group end.// -->
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Register </button>
                    </div> <!-- form-group// -->
                </form>
            </article><!-- card-body.// -->
        </div> <!-- card .// -->
        <p class="text-center mt-4">Have an account? <a href="<?php echo base_url('shop/masuk'); ?>">Log In</a></p>
        <br><br>
        <!-- ============================ COMPONENT REGISTER  END.// ================================= -->


    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->


    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Terms and conditions</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
       $(function() {
		var txt = $("#username");
		var func = function() {
			txt.val(txt.val().replace(/\s/g, ''));
			txt.val(txt.val().toLowerCase());
		}
		txt.keyup(func).blur(func);
	});
    </script>
</body>

</html>