<section class="section">
	<div class="section-header">
		<h1>Halaman User</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah User</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("user/update_action/1/") . $id; ?>" class="needs-validation" novalidate="" oninput="password_conf.setCustomValidity(password_conf.value != password.value ? &quot;Passwords do not match.&quot; : &quot;&quot;)">
								<div class="form-group">
									<label for="nama">Username</label>
									<input id="username" type="text" class="form-control" name="username" value="<?php echo $username; ?>" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Username Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Email</label>
									<input id="email" type="email" class="form-control" name="email" value="<?php echo $email; ?>" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Email Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Nama Lengkap</label>
									<input id="name" type="text" class="form-control" value="<?php echo $name; ?>" name="name" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama Lengkap Masih Kosong
									</div>
								</div>


								<div class="form-group">
									<label for="nama">Nomor (WA)</label>
									<input id="tlp" type="number" class="form-control" value="<?php echo $tlp; ?>" name="tlp" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nomor (WA) Masih Kosong
									</div>
								</div>
								<h2 class="section-title">Lupa Password ?</h2>
								<p class="section-lead">Ubah Password User Admin Disini !</p>
								<div class="form-group">
									<label for="nama">Password</label>
									<input id="password" type="password" class="form-control" name="password" tabindex="1">
									<div class="invalid-feedback">
										Password Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Konfirmasi Password</label>
									<input id="password_conf" type="password" class="form-control" name="password_conf" tabindex="1">
									<div class="invalid-feedback">
										Password Masih Kosong / Password Tidak Sesuai
									</div>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Ubah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data User</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="category_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>
											#Kode
										</th>
										<th>Nama Lengkap</th>
										<th>Username</th>
										<th>Email</th>
										<th>No. Telp</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#category_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('user/user_admin_json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "id"
				},
				{
					"data": "name"
				},
				{
					"data": "username"
				},
				{
					"data": "email"
				},
				{
					"data": "tlp"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("user/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("category/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>