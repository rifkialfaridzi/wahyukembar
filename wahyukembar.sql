-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2022 at 08:59 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wahyukembar`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`id`, `nama`) VALUES
(4, 'Cotton Combed 20s'),
(5, 'Cotton Combed 24s'),
(6, 'Cotton Combed 30s');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `warna` int(11) NOT NULL,
  `bahan` int(11) NOT NULL,
  `ukuran` varchar(10) NOT NULL,
  `supplier` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `berat` float NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `category`, `warna`, `bahan`, `ukuran`, `supplier`, `image`, `stock`, `berat`, `harga_penjualan`, `min_stock`, `note`, `created_at`) VALUES
(26, 'SKU0001', 'Baj Batik Motif', 15, 6, 6, 'All Size', 8, 'a11.jpg', '44', 0.3, '70000', '10', 'Bahan kaos sangat tebal sedikit kaku dan berat dibanding dengan 24s,30s', '2021-07-06'),
(27, 'SKU0002', 'Baju Batik Motif 3', 16, 6, 5, 'All Size', 8, 'a2.jpg', '50', 0.3, '80000', '10', 'Bahan kaos standar tekstur tidak terlalu tipis dan tidak terlalu tebal.', '2021-07-06'),
(28, 'SKU0003', 'Kaos Kaftan', 17, 8, 6, 'M', 7, 'a1.jpeg', '49', 0.3, '90000', '10', 'Memiliki serat yang halus, tidak terlalu tipis', '2021-07-06'),
(29, 'SKU0004', 'Baju Batik Model 2', 17, 9, 5, 'All Size', 8, 'a3.jpg', '48', 0.3, '80000', '10', 'Bahan kaos standar tekstur tidak terlalu tipis dan tidak terlalu tebal.', '2021-07-06'),
(30, 'SKU0005', 'Dress Panjang Hasna', 17, 6, 6, 'S', 8, 'ac40762b-1e60-46ff-8d61-4d2ffbef28be.jpg', '35', 0.3, '90000', '10', 'Bahan kaos standar tekstur tidak terlalu tipis dan tidak terlalu tebal.', '2021-07-06'),
(31, 'SKU0006', 'Batin Tenun Wanita', 16, 6, 6, 'All Size', 8, 'a1.jpg', '100', 0.3, '100000', '10', 'Bahan Adem Bagus Banget', '2022-06-01'),
(32, 'SKU0007', 'Dress Tenun', 17, 7, 5, 'All Size', 8, '28248489_B.jpeg', '100', 1, '120000', '1', 'Dress Tenun', '2022-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(15, 'Kaos Pria'),
(16, 'Kaos Anak'),
(17, 'Kaos Custom');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `send_to` int(11) NOT NULL,
  `send_by` int(11) NOT NULL,
  `content` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `send_to`, `send_by`, `content`, `tanggal`) VALUES
(1, 8, 1, 'Selamat Datang Kak, kami punya Promosi Produk Baru nih', '2021-07-04 12:50:51'),
(2, 8, 1, 'Info Promo Ada di Website Ya', '2021-07-04 12:52:51'),
(3, 1, 8, 'iya kak saya member Baru hehe', '2021-07-04 12:55:18'),
(4, 1, 13, 'Halo kak saya mau custom baju', '2021-07-04 12:53:05'),
(5, 8, 1, 'Halo', '0000-00-00 00:00:00'),
(6, 8, 1, 'Haloa', '0000-00-00 00:00:00'),
(7, 8, 1, 'Bayar WOii', '0000-00-00 00:00:00'),
(8, 8, 1, 'Njir Pengutang', '0000-00-00 00:00:00'),
(9, 8, 1, 'Halo kak bayar dong', '2021-07-04 14:33:04'),
(10, 11, 1, 'Halo kak ardiansyah', '2021-07-04 14:33:47'),
(11, 12, 1, 'Halo kak saya punya promo nih', '2021-07-06 13:55:35'),
(12, 1, 12, 'Halo Kak Saya Mau Tanya', '2021-07-06 15:16:03'),
(13, 1, 12, 'Kalo mau cutom kaos berapa ya?', '2021-07-06 15:16:22'),
(14, 12, 1, 'bisa kak langsung WA CS ya', '2021-07-06 15:20:39'),
(15, 1, 8, 'Halo kak', '2022-06-01 08:18:41'),
(16, 1, 8, 'apakah ready?', '2022-06-01 08:18:55'),
(17, 1, 8, 'Halo kak', '2022-06-01 08:20:54'),
(18, 1, 8, 'cek', '2022-06-01 08:25:30'),
(19, 8, 1, 'Ada kak', '2022-06-01 08:26:32'),
(20, 1, 8, 'oke kak', '2022-06-01 08:27:14'),
(21, 8, 1, 'gud', '2022-06-01 08:27:56');

-- --------------------------------------------------------

--
-- Table structure for table `detail_payment`
--

CREATE TABLE `detail_payment` (
  `id` int(11) NOT NULL,
  `penjualan` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `rekening` int(15) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_payment`
--

INSERT INTO `detail_payment` (`id`, `penjualan`, `payment`, `nama`, `rekening`, `foto`) VALUES
(24, 121, 2, 'Aulia Kusuma', 128736, 'conoth15.PNG'),
(25, 122, 5, 'Aulia Kusuma', 23784, 'conoth16.PNG'),
(26, 123, 2, 'Rifki Alfaridzi', 3121212, 'ceker.jpeg'),
(27, 124, 0, '-', 0, '-'),
(28, 125, 2, 'Namida', 324234, 'a.jpeg'),
(29, 126, 0, '-', 0, '-'),
(30, 127, 0, '-', 0, '-'),
(31, 128, 0, '-', 0, '-');

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE `diskon` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` int(3) NOT NULL COMMENT ' Dalam Bentuk percent',
  `active` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon`
--

INSERT INTO `diskon` (`id`, `nama`, `jumlah`, `active`) VALUES
(2, 'Diskon Syawal', 20, 0),
(4, 'Diskon Ramdhan', 30, 0),
(5, 'Sale 7.7', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurir`
--

INSERT INTO `kurir` (`id`, `alamat`, `harga`) VALUES
(1, 'Surakarta', 10000),
(2, 'Sukoharjo', 8000),
(5, 'madura', 7000);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `nama`, `no_rekening`) VALUES
(1, 'BRI', '51278345628'),
(2, 'BCA', '926734812376'),
(5, 'BNI', '7812534781');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tlp` varchar(15) NOT NULL,
  `alamat_penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `nama`, `tlp`, `alamat_penerima`) VALUES
(27, 'Rifki Alfaridzi', '081393600707', 'Grogol\r\nJalan Seruni RT 01 RW 03 Grogol Sukoharjo'),
(28, 'kasir', 'n/a', 'n/a'),
(29, 'kasir', 'n/a', 'n/a'),
(30, 'Bambang', '081393600939', 'Grogol RT 01 RW 03'),
(31, 'Kak Andin', 'n/a', 'n/a');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `pelanggan` int(11) NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 bDibayar, 1 sDibayar, 2Dikirim, 3 Selesai 4 ditolak',
  `approvement` int(11) NOT NULL COMMENT 'admin yang memverifikasi pembayaran',
  `payment` int(11) NOT NULL,
  `kurir` int(11) NOT NULL,
  `ongkir` varchar(255) NOT NULL,
  `member` enum('ya','tidak') NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `diskon`, `total_penjualan`, `pelanggan`, `status`, `approvement`, `payment`, `kurir`, `ongkir`, `member`, `created_at`) VALUES
(121, 'TRX526797-20210706', 12, 5, '350000', 0, 3, 1, 0, 2, '16000', 'ya', '2021-07-06'),
(122, 'TRX665631-20210706', 12, 5, '350000', 0, 3, 1, 0, 5, '14000', 'ya', '2021-07-06'),
(123, 'TRX566958-20220601', 0, 0, '270000', 27, 3, 1, 0, 5, '7000', 'tidak', '2022-06-01'),
(124, 'TRX941224-20220615', 0, 0, '90000', 28, 3, 1, 0, 0, '0', 'tidak', '2022-06-15'),
(125, 'TRX943206-20220615', 8, 5, '150000', 0, 3, 1, 0, 5, '7000', 'ya', '2022-06-15'),
(126, 'TRX710860-20220615', 0, 0, '90000', 29, 3, 1, 0, 0, '0', 'tidak', '2022-06-15'),
(127, 'TRX848307-20220615', 0, 0, '160000', 30, 0, 0, 0, 2, '8000', 'tidak', '2022-06-15'),
(128, 'TRX472717-20220615', 0, 0, '70000', 31, 3, 1, 0, 0, '0', 'tidak', '2022-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `total`, `penjualan`) VALUES
(167, 30, '2', '180000', 121),
(168, 29, '1', '80000', 121),
(169, 28, '1', '90000', 121),
(170, 26, '5', '350000', 122),
(171, 30, '3', '270000', 123),
(172, 30, '1', '90000', 124),
(173, 26, '1', '70000', 125),
(174, 29, '1', '80000', 125),
(175, 30, '1', '90000', 126),
(176, 30, '1', '90000', 127),
(177, 26, '1', '70000', 127),
(178, 26, '1', '70000', 128);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(7, 'SP420109', 'Grosir Batik Solo', 'PGS Blok S', '08137462373', '2020-03-25'),
(8, 'SP497763', 'Grosir Batik Grogol 1', 'Grogol', '081363599878', '2020-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `tlp` varchar(15) NOT NULL,
  `token` varchar(5) NOT NULL,
  `recovery` int(2) NOT NULL,
  `token_verifikasi` text NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `tlp`, `token`, `recovery`, `token_verifikasi`, `created_at`) VALUES
(1, 'admingudang', 'Samsudin', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '', '', 0, '', '2020-03-21'),
(5, 'adminkasir', 'Rifki Alfaridzi', 'rifki@mai.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '081393600808', '', 0, '', '2020-05-26'),
(6, 'asasiti', 'asasiti auliani', 'rifkis@mai.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '081393600807', '', 0, '', '2020-05-26'),
(8, 'namidasan', 'namida san', 'namidasan@mai.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '6281393600707', '17397', 0, '', '2020-05-27'),
(10, 'reza', 'reza', 'aryareza54@yahoo.com', 'bb98b1d0b523d5e783f931550d7702b6', '', 3, '628995772244', '', 0, '', '2020-06-01'),
(11, 'ardiansyah', 'ardiansyah', 'ardiansyah@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '628337587798', '', 0, '', '2020-06-29'),
(12, 'aliakusuma', 'Alia Kusuma', 'aliakusuma@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '081393600707', '', 0, '', '2021-06-30'),
(13, 'zulfafebriana', 'Zulfa Febriana', 'zulfafebri3@gmail.com', '3fc0a7acf087f549ac2b266baf94b8b1', '', 3, '081393600909', '', 0, '', '2021-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `alamat_penerima` varchar(255) NOT NULL,
  `kurir` int(11) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `user`, `alamat_penerima`, `kurir`, `nama_penerima`) VALUES
(1, 8, 'jalan seruni grogol sukoharjo rt 01 rw 4', 1, 'namida sans'),
(2, 9, '-', 0, 'reza'),
(3, 10, '-', 0, 'reza'),
(4, 11, 'Tipes, Surakarta', 1, 'ardiansyah'),
(5, 12, 'Grogol Sukoharjo', 2, 'Alia Kusuma'),
(6, 13, 'Grogol Sukoharjo', 0, 'Zulfa Febriana');

-- --------------------------------------------------------

--
-- Table structure for table `warna`
--

CREATE TABLE `warna` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `warna`
--

INSERT INTO `warna` (`id`, `nama`) VALUES
(6, 'Hitam'),
(7, 'Putih'),
(8, 'Hijau'),
(9, 'Merah');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `warna` (`warna`),
  ADD KEY `bahan` (`bahan`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`send_to`),
  ADD KEY `send_by` (`send_by`);

--
-- Indexes for table `detail_payment`
--
ALTER TABLE `detail_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`),
  ADD KEY `payment` (`payment`);

--
-- Indexes for table `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `diskon` (`diskon`),
  ADD KEY `kurir` (`kurir`),
  ADD KEY `payment` (`payment`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `kurir` (`kurir`);

--
-- Indexes for table `warna`
--
ALTER TABLE `warna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `detail_payment`
--
ALTER TABLE `detail_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `diskon`
--
ALTER TABLE `diskon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kurir`
--
ALTER TABLE `kurir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warna`
--
ALTER TABLE `warna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
